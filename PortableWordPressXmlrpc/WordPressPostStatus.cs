﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// A WordPress post status
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostStatus : KeyValueBase
	{
	}
}

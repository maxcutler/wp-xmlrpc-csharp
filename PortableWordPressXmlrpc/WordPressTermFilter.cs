﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Filter options for term methods.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressTermFilter
	{
		/// <summary>
		/// Gets or sets the search text.
		/// </summary>
		/// <value>The search text.</value>
		[XmlRpcMember(Name = "search", Optional = true)]
		public string Search { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether to include terms with no posts.
		/// </summary>
		/// <value>
		///   <c>true</c> if hide terms with no posts; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "hide_empty", Optional = true)]
		public bool HideEmpty { get; set; }

		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>The number.</value>
		[XmlRpcMember(Name = "number", Optional = true)]
		public int Number { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>The offset.</value>
		[XmlRpcMember(Name = "offset", Optional = true)]
		public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the ordering field.
		/// </summary>
		/// <value>The ordering field.</value>
		[XmlRpcMember(Name = "orderby", Optional = true)]
		public string OrderBy { get; set; }

		/// <summary>
		/// Gets or sets the ordering direction.
		/// </summary>
		/// <value>The ordering direction.</value>
		[XmlRpcMember(Name = "order", Optional = true)]
		public string OrderDirection { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Filter options for media methods.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressMediaFilter
	{
		/// <summary>
		/// Gets or sets the MIME type of the media item.
		/// </summary>
		/// <value>The MIME type of the media item.</value>
		[XmlRpcMember(Name = "mime_type", Optional = true)]
		public string MimeType { get; set; }

		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>The number.</value>
		[XmlRpcMember(Name = "number", Optional = true)]
		public int Number { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>The offset.</value>
		[XmlRpcMember(Name = "offset", Optional = true)]
		public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the parent post.
		/// </summary>
		/// <value>The parent post.</value>
		[XmlRpcMember(Name = "parent_id", Optional = true)]
		public string Parent { get; set; }
	}
}

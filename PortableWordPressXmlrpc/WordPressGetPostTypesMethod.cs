﻿namespace PortableWordPressXmlrpc
{
	using System.Linq;
	using System.Xml;
	using System.Xml.Linq;

	using PortableXmlrpc;

	/// <summary>
	/// XML-RPC method to get WordPress post types.
	/// WordPress returns a hash-table, which is not supported by PortableXmlrpc.
	/// This class transforms the struct into a list by discarding the redundant keys.
	/// </summary>
	public class WordPressGetPostTypesMethod : XmlRpcMethod
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="WordPressGetPostTypesMethod" /> class.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <param name="parameters">The parameters.</param>
		public WordPressGetPostTypesMethod(string name, params object[] parameters)
			: base(name, parameters)
		{
		}

		/// <summary>
		/// Transforms the value from a hash-table to an array.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>Transformed XElement.</returns>
		protected override XElement TransformValue(XElement value)
		{
			var structValue = value.Element("struct");
			if (structValue == null)
			{
				throw new XmlException("Malformed XML received from XML-RPC server.");
			}

			return new XElement(
				"value",
				new XElement("array", new XElement("data", structValue.Elements("member").Select(x => x.Element("value")))));
		}
	}
}

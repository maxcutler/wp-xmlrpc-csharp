﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Metadata for a WordPress media item.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressMediaItemMetadata
	{
		/// <summary>
		/// Gets or sets the image width.
		/// </summary>
		/// <value>The image width.</value>
		[XmlRpcMember(Name = "width", Optional = true)]
		public int Width { get; set; }

		/// <summary>
		/// Gets or sets the image height.
		/// </summary>
		/// <value>The image height.</value>
		[XmlRpcMember(Name = "height", Optional = true)]
		public int Height { get; set; }

		/// <summary>
		/// Filename of the full-size media item, including path from the uploads directory.
		/// </summary>
		[XmlRpcMember(Name = "file", Optional = true)]
		public string Filename { get; set; }

		/// <summary>
		/// The available sizes for the media item.
		/// </summary>
		[XmlRpcMember(Name = "sizes", Optional = true)]
		public WordPressMediaItemSizes Sizes { get; set; }
	}
}

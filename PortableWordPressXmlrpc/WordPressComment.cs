﻿namespace PortableWordPressXmlrpc
{
	using System;

	using PortableXmlrpc;

	[XmlRpcSerializable]
	public class WordPressComment
	{
		/// <summary>
		/// Gets or sets the comment ID.
		/// </summary>
		[XmlRpcMember(Name = "comment_id", Optional = true)]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the parent comment ID.
		/// </summary>
		[XmlRpcMember(Name = "parent", Optional = true)]
		public string Parent { get; set; }

		/// <summary>
		/// Gets or sets the user ID.
		/// </summary>
		[XmlRpcMember(Name = "user_id", Optional = true)]
		public string User { get; set; }

		/// <summary>
		/// Gets or sets the post ID.
		/// </summary>
		[XmlRpcMember(Name = "post_id", Optional = true)]
		public string Post { get; set; }

		/// <summary>
		/// Gets or sets the created date time.
		/// </summary>
		[XmlRpcMember(Name = "date_created_gmt", Optional = true)]
		public DateTime CreatedDateTime { get; set; }

		/// <summary>
		/// Gets or sets the comment status.
		/// </summary>
		[XmlRpcMember(Name = "status", Optional = true)]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the comment content.
		/// </summary>
		[XmlRpcMember(Name = "content", Optional = true)]
		public string Content { get; set; }

		/// <summary>
		/// Gets or sets the comment link.
		/// </summary>
		[XmlRpcMember(Name = "link", Optional=true)]
		public string Link { get; set; }

		/// <summary>
		/// Gets or sets the post title.
		/// </summary>
		[XmlRpcMember(Name = "post_title", Optional = true)]
		public string PostTitle { get; set; }

		/// <summary>
		/// Gets or sets the author's name.
		/// </summary>
		[XmlRpcMember(Name = "author", Optional = true)]
		public string AuthorName { get; set; }

		/// <summary>
		/// Gets or sets the author's URL.
		/// </summary>
		[XmlRpcMember(Name = "author_url", Optional = true)]
		public string AuthorUrl { get; set; }

		/// <summary>
		/// Gets or sets the author's email address.
		/// </summary>
		[XmlRpcMember(Name = "author_email", Optional = true)]
		public string AuthorEmail { get; set; }

		/// <summary>
		/// Gets or sets the author's IP address.
		/// </summary>
		[XmlRpcMember(Name = "author_ip", Optional = true)]
		public string AuthorIpAddress { get; set; }

		/// <summary>
		/// Gets or sets the type of the comment.
		/// This should be "pingback", "trackback", or empty/null.
		/// </summary>
		[XmlRpcMember(Name = "type", Optional = true)]
		public string CommentType { get; set; }
	}
}

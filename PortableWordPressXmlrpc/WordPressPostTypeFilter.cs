﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Filter options for post type methods.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostTypeFilter
	{
		/// <summary>
		/// Gets or sets a value indicating whether to include only public post types.
		/// </summary>
		/// <value>
		///   <c>true</c> if show only public post types; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "public", Optional = true)]
		public bool Public { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// A WordPress term.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressTerm
	{
		/// <summary>
		/// Gets or sets the term ID.
		/// </summary>
		/// <value>The term ID.</value>
		[XmlRpcMember(Name = "term_id", Optional = true)]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the term name.
		/// </summary>
		/// <value>The term name.</value>
		[XmlRpcMember(Name = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the term slug.
		/// </summary>
		/// <value>The term name.</value>
		[XmlRpcMember(Name = "slug", Optional = true)]
		public string Slug { get; set; }

		/// <summary>
		/// Gets or sets the term taxonomy.
		/// </summary>
		/// <value>The term taxonomy.</value>
		[XmlRpcMember(Name = "taxonomy")]
		public string Taxonomy { get; set; }

		/// <summary>
		/// Gets or sets the term description.
		/// </summary>
		/// <value>The term description.</value>
		[XmlRpcMember(Name = "description", Optional = true)]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the term parent.
		/// </summary>
		/// <value>The term parent.</value>
		[XmlRpcMember(Name = "parent", Optional = true)]
		public string Parent { get; set; }

		/// <summary>
		/// Gets or sets the term post count.
		/// </summary>
		/// <value>The term post count.</value>
		[XmlRpcMember(Name = "count", Optional = true, SerializationMode = XmlRpcMemberSerializationMode.DeserializeOnly)]
		public int Count { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// UI labels for a WordPress taxonomy.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressTaxonomyLabels
	{
		/// <summary>
		/// Gets or sets the singular name.
		/// </summary>
		/// <value>The singular name.</value>
		[XmlRpcMember(Name = "singular_name")]
		public string SingularName { get; set; }

		/// <summary>
		/// Gets or sets the plural name.
		/// </summary>
		/// <value>The plural name.</value>
		[XmlRpcMember(Name = "name")]
		public string PluralName { get; set; }

		/// <summary>
		/// Gets or sets the search label.
		/// </summary>
		/// <value>The search label.</value>
		[XmlRpcMember(Name = "search_items")]
		public string SearchItems { get; set; }

		/// <summary>
		/// Gets or sets the popular label.
		/// </summary>
		/// <value>The popular label.</value>
		[XmlRpcMember(Name = "popular_items")]
		public string PopularItems { get; set; }

		/// <summary>
		/// Gets or sets the all items label.
		/// </summary>
		/// <value>The all items label.</value>
		[XmlRpcMember(Name = "all_items")]
		public string AllItems { get; set; }

		/// <summary>
		/// Gets or sets the parent label.
		/// </summary>
		/// <value>The parent label.</value>
		[XmlRpcMember(Name = "parent_item")]
		public string ParentItem { get; set; }

		/// <summary>
		/// Gets or sets the edit label.
		/// </summary>
		/// <value>The edit label.</value>
		[XmlRpcMember(Name = "edit_item")]
		public string EditItem { get; set; }

		/// <summary>
		/// Gets or sets the view label.
		/// </summary>
		/// <value>The view label.</value>
		[XmlRpcMember(Name = "view_item")]
		public string ViewItem { get; set; }

		/// <summary>
		/// Gets or sets the add new label.
		/// </summary>
		/// <value>The add new label.</value>
		[XmlRpcMember(Name = "add_new_item")]
		public string AddNewItem { get; set; }

		/// <summary>
		/// Gets or sets the new item name label.
		/// </summary>
		/// <value>The new item name label.</value>
		[XmlRpcMember(Name = "new_item_name")]
		public string NewItemName { get; set; }

		/// <summary>
		/// Gets or sets the add/remove label.
		/// </summary>
		/// <value>The add/remove label.</value>
		[XmlRpcMember(Name = "add_or_remove_items")]
		public string AddOrRemoveItems { get; set; }

		/// <summary>
		/// Gets or sets the choose most used label.
		/// </summary>
		/// <value>The choose most used label.</value>
		[XmlRpcMember(Name = "choose_from_most_used")]
		public string ChooseMostUsed { get; set; }
	}
}

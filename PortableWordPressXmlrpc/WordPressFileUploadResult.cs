﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	[XmlRpcSerializable]
	public class WordPressFileUploadResult
	{
		[XmlRpcMember(Name = "id")]
		public string Id { get; set; }

		[XmlRpcMember(Name = "file")]
		public string Filename { get; set; }

		[XmlRpcMember(Name = "url")]
		public string Url { get; set; }

		[XmlRpcMember(Name = "type")]
		public string FileType { get; set; }
	}
}

﻿
namespace PortableWordPressXmlrpc
{
	using System.Linq;
	using System.Xml;
	using System.Xml.Linq;

	using PortableXmlrpc;

	public class KeyValueStructMethod : XmlRpcMethod
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="KeyValueStructMethod" /> class.
		/// WordPress often returns hash-tables, which are not supported by PortableXmlrpc.
		/// This class transforms the struct into a list.
		/// </summary>
		/// <param name="name">The method name.</param>
		/// <param name="parameters">The method parameters.</param>
		public KeyValueStructMethod(string name, params object[] parameters)
			: base(name, parameters)
		{
		}

		/// <summary>
		/// Transforms the value.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>Transformed XElement.</returns>
		protected override XElement TransformValue(XElement value)
		{
			var structValue = value.Element("struct");
			if (structValue == null)
			{
				throw new XmlException("Malformed XML received from XML-RPC server.");
			}

			var pairs = structValue.Elements("member").ToDictionary(x => x.Element("name").Value, x => x.Element("value"));
			var arrayData = new XElement("data");
			foreach (var pair in pairs)
			{
				arrayData.Add(
					new XElement(
						"value",
						new XElement(
							"struct",
							new XElement("member", new XElement("name", "name"), new XElement("value", new XElement("string", pair.Key))),
							new XElement("member", new XElement("name", "label"), pair.Value))));
			}

			return new XElement("value", new XElement("array", arrayData));
		}
	}
}

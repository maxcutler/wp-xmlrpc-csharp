﻿namespace PortableWordPressXmlrpc
{
	using System;

	using PortableXmlrpc;

	[XmlRpcSerializable]
	public class WordPressFileUpload
	{
		[XmlRpcMember(Name = "name")]
		public string Filename { get; set; }

		[XmlRpcMember(Name = "type")]
		public string MimeType { get; set; }

		[XmlRpcMember(Name = "post_id", Optional = true)]
		public string PostId { get; set; }

		[XmlRpcMember(Name = "bits")]
		public byte[] Bits { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// UI labels for a WordPress post type.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostTypeLabels
	{
		/// <summary>
		/// Gets or sets the singular name.
		/// </summary>
		/// <value>The singular name.</value>
		[XmlRpcMember(Name = "singular_name")]
		public string SingularName { get; set; }

		/// <summary>
		/// Gets or sets the plural name.
		/// </summary>
		/// <value>The plural name.</value>
		[XmlRpcMember(Name = "name")]
		public string PluralName { get; set; }

		/// <summary>
		/// Gets or sets the search label.
		/// </summary>
		/// <value>The search label.</value>
		[XmlRpcMember(Name = "search_items")]
		public string SearchItems { get; set; }

		/// <summary>
		/// Gets or sets the add new label.
		/// </summary>
		/// <value>The add new label.</value>
		[XmlRpcMember(Name = "add_new")]
		public string AddNew { get; set; }

		/// <summary>
		/// Gets or sets the all items label.
		/// </summary>
		/// <value>The all items label.</value>
		[XmlRpcMember(Name = "all_items")]
		public string AllItems { get; set; }

		/// <summary>
		/// Gets or sets the parent label.
		/// </summary>
		/// <value>The parent label.</value>
		[XmlRpcMember(Name = "parent_item_colon")]
		public string ParentItem { get; set; }

		/// <summary>
		/// Gets or sets the edit label.
		/// </summary>
		/// <value>The edit label.</value>
		[XmlRpcMember(Name = "edit_item")]
		public string EditItem { get; set; }

		/// <summary>
		/// Gets or sets the view label.
		/// </summary>
		/// <value>The view label.</value>
		[XmlRpcMember(Name = "view_item")]
		public string ViewItem { get; set; }

		/// <summary>
		/// Gets or sets the add new label.
		/// </summary>
		/// <value>The add new label.</value>
		[XmlRpcMember(Name = "add_new_item")]
		public string AddNewItem { get; set; }

		/// <summary>
		/// Gets or sets the new item label.
		/// </summary>
		/// <value>The new item label.</value>
		[XmlRpcMember(Name = "new_item")]
		public string NewItem { get; set; }

		/// <summary>
		/// Gets or sets the not found label.
		/// </summary>
		/// <value>The not found label.</value>
		[XmlRpcMember(Name = "not_found")]
		public string NotFound { get; set; }

		/// <summary>
		/// Gets or sets the not found in trash label.
		/// </summary>
		/// <value>The not found in trash label.</value>
		[XmlRpcMember(Name = "not_found_in_trash")]
		public string NotFoundInTrash { get; set; }

		/// <summary>
		/// Gets or sets the menu name label.
		/// </summary>
		/// <value>The menu name label.</value>
		[XmlRpcMember(Name = "menu_name")]
		public string MenuName { get; set; }
	}
}

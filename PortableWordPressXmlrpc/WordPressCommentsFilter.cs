﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	[XmlRpcSerializable]
	public class WordPressCommentsFilter
	{
		/// <summary>
		/// Gets or sets the post ID.
		/// If null, all comments will be retrieved.
		/// </summary>
		[XmlRpcMember(Name = "post_id", Optional = true)]
		public string PostId { get; set; }

		/// <summary>
		/// Gets or sets the comment status.
		/// </summary>
		[XmlRpcMember(Name = "status", Optional = true)]
		public string CommentStatus { get; set; }

		/// <summary>
		/// Gets or sets the number of comments to return.
		/// </summary>
		[XmlRpcMember(Name = "number", Optional = true)]
		public int? Number { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		[XmlRpcMember(Name = "offset", Optional = true)]
		public int? Offset { get; set; }
	}
}

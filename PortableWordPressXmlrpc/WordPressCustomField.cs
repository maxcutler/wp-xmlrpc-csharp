﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// A WordPress post custom field.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressCustomField
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		[XmlRpcMember(Name = "id", Optional = true)]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the field name.
		/// </summary>
		/// <value>The field name.</value>
		[XmlRpcMember(Name = "key")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[XmlRpcMember(Name = "value")]
		public string Value { get; set; }
	}
}

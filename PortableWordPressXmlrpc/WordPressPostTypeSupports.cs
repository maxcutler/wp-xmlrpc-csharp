﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	[XmlRpcSerializable]
	public class WordPressPostTypeSupports
	{
		/// <summary>
		/// Gets or sets a value indicating whether this type supports titles.
		/// </summary>
		/// <value>
		///   <c>true</c> if title is supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "title")]
		public bool Title { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports editor.
		/// </summary>
		/// <value>
		///   <c>true</c> if editor is supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "editor")]
		public bool Editor { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports author.
		/// </summary>
		/// <value>
		///   <c>true</c> if author is supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "author")]
		public bool Author { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports thumbnail.
		/// </summary>
		/// <value>
		///   <c>true</c> if thumbnail is supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "thumbnail")]
		public bool Thumbnail { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports excerpt.
		/// </summary>
		/// <value>
		///   <c>true</c> if excerpt is supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "excerpt")]
		public bool Excerpt { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports trackbacks.
		/// </summary>
		/// <value>
		///   <c>true</c> if trackbacks are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "trackbacks")]
		public bool Trackbacks { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports custom fields.
		/// </summary>
		/// <value>
		///   <c>true</c> if custom fields are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "custom-fields")]
		public bool CustomFields { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports comments.
		/// </summary>
		/// <value>
		///   <c>true</c> if comments are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "comments")]
		public bool Comments { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports revisions.
		/// </summary>
		/// <value>
		///   <c>true</c> if revisions are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "revisions")]
		public bool Revisions { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports page attributes.
		/// </summary>
		/// <value>
		///   <c>true</c> if page attributes are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "page-attributes")]
		public bool PageAttributes { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this type supports post formats.
		/// </summary>
		/// <value>
		///   <c>true</c> if post formats are supported; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "post-formats")]
		public bool PostFormats { get; set; }
	}
}

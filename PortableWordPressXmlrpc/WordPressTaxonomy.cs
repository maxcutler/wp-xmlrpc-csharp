﻿namespace PortableWordPressXmlrpc
{
	using System.Collections.Generic;

	using PortableXmlrpc;

	/// <summary>
	/// A WordPress term.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressTaxonomy
	{
		/// <summary>
		/// Gets or sets the taxonomy name.
		/// </summary>
		/// <value>The taxonomy name.</value>
		[XmlRpcMember(Name = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the taxonomy label.
		/// </summary>
		/// <value>The taxonomy label.</value>
		[XmlRpcMember(Name = "label")]
		public string Label { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this taxonomy is hierarchical.
		/// </summary>
		/// <value>
		/// <c>true</c> if this taxonomy is hierarchical; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "hierarchical")]
		public bool IsHierarchical { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this taxonomy is public.
		/// </summary>
		/// <value>
		/// <c>true</c> if this taxonomy is public; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "public")]
		public bool IsPublic { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this taxonomy is shown in UI.
		/// </summary>
		/// <value>
		/// <c>true</c> if this taxonomy is shown in UI; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "show_ui")]
		public bool ShowUI { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this taxonomy is built-in.
		/// </summary>
		/// <value>
		/// <c>true</c> if this taxonomy is built-in; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "_builtin")]
		public bool IsBuiltin { get; set; }

		/// <summary>
		/// Gets or sets the post types.
		/// </summary>
		/// <value>The post types.</value>
		[XmlRpcMember(Name = "object_type", Optional = true)]
		public List<string> PostTypes { get; set; }

		/// <summary>
		/// Gets or sets the labels.
		/// </summary>
		/// <value>The labels.</value>
		[XmlRpcMember(Name = "labels")]
		public WordPressTaxonomyLabels Labels { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using System;

	using PortableXmlrpc;

	/// <summary>
	/// A WordPress media item (aka attachment).
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressMediaItem
	{
		/// <summary>
		/// Gets or sets the id.
		/// </summary>
		/// <value>The id.</value>
		[XmlRpcMember(Name = "attachment_id", Optional = true)]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the parent post.
		/// </summary>
		/// <value>The parent post.</value>
		[XmlRpcMember(Name = "parent")]
		public string Parent { get; set; }

		/// <summary>
		/// Gets or sets the GMT creation date.
		/// </summary>
		/// <value>The post date.</value>
		[XmlRpcMember(Name = "date_created_gmt")]
		public DateTime Uploaded { get; set; }

		/// <summary>
		/// Gets or sets the attachment link.
		/// </summary>
		/// <value>The attachment link.</value>
		[XmlRpcMember(Name = "link")]
		public string Link { get; set; }

		/// <summary>
		/// Gets or sets the attachment thumbnail.
		/// </summary>
		/// <value>The attachment thumbnail.</value>
		[XmlRpcMember(Name = "thumbnail")]
		public string Thumbnail { get; set; }

		/// <summary>
		/// Gets or sets the attachment title.
		/// </summary>
		/// <value>The attachment title.</value>
		[XmlRpcMember(Name = "title")]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the attachment caption.
		/// </summary>
		/// <value>The attachment caption.</value>
		[XmlRpcMember(Name = "caption")]
		public string Caption { get; set; }

		/// <summary>
		/// Gets or sets the attachment description.
		/// </summary>
		/// <value>The attachment description.</value>
		[XmlRpcMember(Name = "description")]
		public string Description { get; set; }

		/// <summary>
		/// Gets or sets the attachment metadata.
		/// </summary>
		/// <value>The attachment metadata.</value>
		[XmlRpcMember(Name = "metadata", Optional = true)]
		public WordPressMediaItemMetadata Metadata { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using System;
	using System.Collections.Generic;

	using PortableXmlrpc;

	/// <summary>
	/// A WordPress post.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPost
	{
		/// <summary>
		/// Gets or sets the post ID.
		/// </summary>
		/// <value>The post ID.</value>
		[XmlRpcMember(Name = "post_id", Optional = true)]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the post title.
		/// </summary>
		/// <value>The post title.</value>
		[XmlRpcMember(Name = "post_title", Optional = true)]
		public string Title { get; set; }

		/// <summary>
		/// Gets or sets the post date in blog time-zone.
		/// </summary>
		/// <value>The post date.</value>
		[XmlRpcMember(Name = "post_date", Optional = true)]
		public DateTime? LocalDateTime { get; set; }

		/// <summary>
		/// Gets or sets the GMT post date.
		/// </summary>
		/// <value>The post date.</value>
		[XmlRpcMember(Name = "post_date_gmt", Optional = true)]
		public DateTime? GmtDateTime { get; set; }

		/// <summary>
		/// Gets or sets the post modification date in blog time-zone.
		/// </summary>
		/// <value>The post date.</value>
		[XmlRpcMember(Name = "post_modified", Optional = true)]
		public DateTime? LocalModifiedDateTime { get; set; }

		/// <summary>
		/// Gets or sets the GMT post modification date.
		/// </summary>
		/// <value>The post date.</value>
		[XmlRpcMember(Name = "post_modified_gmt", Optional = true)]
		public DateTime? GmtModifiedDateTime { get; set; }

		/// <summary>
		/// Gets or sets the flag to force saving of a new date, even if the post is a draft.
		/// </summary>
		[XmlRpcMember(Name = "edit_date", Optional = true)]
		public bool? EditDate { get; set; }

		/// <summary>
		/// Gets or sets the post status.
		/// </summary>
		/// <value>The post status.</value>
		[XmlRpcMember(Name = "post_status", Optional = true)]
		public string Status { get; set; }

		/// <summary>
		/// Gets or sets the post type.
		/// </summary>
		/// <value>The post type.</value>
		[XmlRpcMember(Name = "post_type", Optional = true)]
		public string Type { get; set; }

		/// <summary>
		/// Gets or sets the post format.
		/// </summary>
		/// <value>The post format.</value>
		[XmlRpcMember(Name = "post_format", Optional = true)]
		public string Format { get; set; }

		/// <summary>
		/// Gets or sets the post slug.
		/// </summary>
		/// <value>The post slug.</value>
		[XmlRpcMember(Name = "post_name", Optional = true)]
		public string Slug { get; set; }

		/// <summary>
		/// Gets or sets the post author.
		/// </summary>
		/// <value>The post author.</value>
		[XmlRpcMember(Name = "post_author", Optional = true)]
		public string Author { get; set; }

		/// <summary>
		/// Gets or sets the post password.
		/// </summary>
		/// <value>The post password.</value>
		[XmlRpcMember(Name = "post_password", Optional = true)]
		public string Password { get; set; }

		/// <summary>
		/// Gets or sets the post excerpt.
		/// </summary>
		/// <value>The post excerpt.</value>
		[XmlRpcMember(Name = "post_excerpt", Optional = true)]
		public string Excerpt { get; set; }

		/// <summary>
		/// Gets or sets the post content.
		/// </summary>
		/// <value>The post content.</value>
		[XmlRpcMember(Name = "post_content", Optional = true)]
		public string Content { get; set; }

		/// <summary>
		/// Gets or sets the post parent.
		/// </summary>
		/// <value>The post parent.</value>
		[XmlRpcMember(Name = "post_parent", Optional = true)]
		public string Parent { get; set; }

		/// <summary>
		/// Gets or sets the post MIME type.
		/// </summary>
		/// <value>The post MIME type.</value>
		[XmlRpcMember(Name = "post_mime_type", Optional = true)]
		public string MimeType { get; set; }

		/// <summary>
		/// Gets or sets the post link.
		/// </summary>
		/// <value>The post link.</value>
		[XmlRpcMember(Name = "link", Optional = true)]
		public string Link { get; set; }

		/// <summary>
		/// Gets or sets the post GUID.
		/// </summary>
		/// <value>The post GUID.</value>
		[XmlRpcMember(Name = "guid", Optional = true)]
		public string Guid { get; set; }

		/// <summary>
		/// Gets or sets the post menu order.
		/// </summary>
		/// <value>The post menu order.</value>
		[XmlRpcMember(Name = "menu_order", Optional = true)]
		public int MenuOrder { get; set; }

		/// <summary>
		/// Gets or sets the post comment status.
		/// </summary>
		/// <value>The post comment status.</value>
		[XmlRpcMember(Name = "comment_status", Optional = true)]
		public string CommentStatus { get; set; }

		/// <summary>
		/// Gets or sets the post ping status.
		/// </summary>
		/// <value>The post ping status.</value>
		[XmlRpcMember(Name = "ping_status", Optional = true)]
		public string PingStatus { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post is sticky.
		/// </summary>
		/// <value>
		///   <c>true</c> if this post is sticky; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "sticky", Optional = true)]
		public bool? IsSticky { get; set; }

		/// <summary>
		/// Gets or sets the thumbnail.
		/// </summary>
		/// <value>The thumbnail.</value>
		[XmlRpcMember(Name = "post_thumbnail", Optional = true, SerializationMode = XmlRpcMemberSerializationMode.DeserializeOnly)]
		public WordPressMediaItem Thumbnail { get; set; }

		/// <summary>
		/// Gets or sets the thumbnail ID.
		/// Should only be used for newPost/editPost.
		/// </summary>
		[XmlRpcMember(Name = "post_thumbnail", Optional = true, SerializationMode = XmlRpcMemberSerializationMode.SerializeOnly)]
		public string ThumbnailId { get; set; }

		/// <summary>
		/// Gets or sets the post terms.
		/// </summary>
		/// <value>The post terms.</value>
		[XmlRpcMember(Name = "terms", Optional = true, SerializationMode = XmlRpcMemberSerializationMode.DeserializeOnly)]
		public List<WordPressTerm> Terms { get; set; }

		/// <summary>
		/// Gets or sets the list of post term IDs.
		/// Should only be used for newPost/editPost.
		/// </summary>
		[XmlRpcMember(Name = "terms", Optional = true, SerializationMode = XmlRpcMemberSerializationMode.SerializeOnly)]
		public Dictionary<string, List<string>> TermsById { get; set; }

			/// <summary>
		/// Gets or sets the post custom fields.
		/// </summary>
		/// <value>The post custom fields.</value>
		[XmlRpcMember(Name = "custom_fields", Optional = true)]
		public List<WordPressCustomField> CustomFields { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using System;
	using System.Collections.Generic;
	using System.Linq;
	using System.Net.Http;
	using System.Threading.Tasks;

	using PortableXmlrpc;

	/// <summary>
	/// XML-RPC client for WordPress server.
	/// </summary>
	public class WordPressClient : XmlRpcClient
	{
		/// <summary>
		/// The blog id.
		/// </summary>
		private readonly int blogId;

		/// <summary>
		/// The username.
		/// </summary>
		private readonly string username;

		/// <summary>
		/// The password.
		/// </summary>
		private readonly string password;

		/// <summary>
		/// Initializes a new instance of the <see cref="WordPressClient" /> class.
		/// </summary>
		/// <param name="serverUri">The server URI.</param>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="httpClient">The HTTP client.</param>
		public WordPressClient(Uri serverUri, string username, string password, HttpClient httpClient)
			: base(serverUri, httpClient)
		{
			this.username = username;
			this.password = password;
			this.blogId = 0;
		}

		/// <summary>
		/// Gets the username.
		/// </summary>
		public string Username
		{
			get { return this.username; }
		}

		/// <summary>
		/// Gets the password.
		/// </summary>
		public string Password
		{
			get { return this.password; }
		}

		#region User Methods

		/// <summary>
		/// Gets the users blogs.
		/// </summary>
		/// <returns>List of blogs.</returns>
		public async Task<List<WordPressBlog>> GetUsersBlogs()
		{
			var wpGetUsersBlogs = new XmlRpcMethod("wp.getUsersBlogs", this.username, this.password);
			return await wpGetUsersBlogs.Execute<List<WordPressBlog>>(this);
		}

		/// <summary>
		/// Gets the profile for the current user.
		/// </summary>
		/// <returns>A WordPressUser.</returns>
		public async Task<WordPressUser> GetProfile()
		{
			var wpGetProfile = new XmlRpcMethod("wp.getProfile", this.blogId, this.username, this.password);
			return await wpGetProfile.Execute<WordPressUser>(this);
		}

		/// <summary>
		/// Gets a user.
		/// </summary>
		/// <param name="id">The user id.</param>
		/// <returns>A WordPressUser.</returns>
		public async Task<WordPressUser> GetUser(string id)
		{
			var wpGetUser = new XmlRpcMethod("wp.getUser", this.blogId, this.username, this.password, id);
			return await wpGetUser.Execute<WordPressUser>(this);
		}

		/// <summary>
		/// Gets users from the blog.
		/// </summary>
		/// <param name="filter">User filters.</param>
		/// <returns>List of users.</returns>
		public async Task<List<WordPressUser>> GetUsers(WordPressUserFilter filter = null)
		{
			var wpGetUsers = new XmlRpcMethod("wp.getUsers", this.blogId, this.username, this.password, filter);
			return await wpGetUsers.Execute<List<WordPressUser>>(this);
		}

		#endregion

		#region Post Methods

		/// <summary>
		/// Gets a post.
		/// </summary>
		/// <param name="id">The post id.</param>
		/// <returns>A WordPressPost.</returns>
		public async Task<WordPressPost> GetPost(string id)
		{
			var wpGetPost = new XmlRpcMethod("wp.getPost", this.blogId, this.username, this.password, id);
			return await wpGetPost.Execute<WordPressPost>(this);
		}

		/// <summary>
		/// Gets posts from the blog.
		/// </summary>
		/// <param name="filter">Query filters.</param>
		/// <returns>List of posts.</returns>
		public async Task<List<WordPressPost>> GetPosts(WordPressPostFilter filter = null)
		{
			var wpGetPosts = new XmlRpcMethod("wp.getPosts", this.blogId, this.username, this.password, filter);
			return await wpGetPosts.Execute<List<WordPressPost>>(this);
		}

		/// <summary>
		/// Saves a new post.
		/// </summary>
		/// <param name="post">The WordPress post.</param>
		/// <returns>Task for completion of the work. The post will be updated with its new ID once complete.</returns>
		public async Task NewPost(WordPressPost post)
		{
			var wpNewPost = new XmlRpcMethod("wp.newPost", this.blogId, this.username, this.password, post);
			var postId = await wpNewPost.Execute<string>(this);
			post.Id = postId;
		}

		/// <summary>
		/// Edits a post.
		/// </summary>
		/// <param name="post">The WordPress post.</param>
		/// <returns>Task for completion of the work.</returns>
		public async Task EditPost(WordPressPost post)
		{
			var wpEditPost = new XmlRpcMethod("wp.editPost", this.blogId, this.username, this.password, post.Id, post);
			await wpEditPost.Execute<bool>(this);
		}

		/// <summary>
		/// Deletes a post.
		/// </summary>
		/// <param name="post">The WordPress post.</param>
		/// <returns>Task for completion of the work.</returns>
		public async Task DeletePost(WordPressPost post)
		{
			var wpDeletePost = new XmlRpcMethod("wp.deletePost", this.blogId, this.username, this.password, post.Id);
			await wpDeletePost.Execute<bool>(this);
		}

		/// <summary>
		/// Gets a post type.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>A WordPressPostType.</returns>
		public async Task<WordPressPostType> GetPostType(string name)
		{
			var fields = new[] { "labels", "menu", "taxonomies" };
			var wpGetPostType = new XmlRpcMethod("wp.getPostType", this.blogId, this.username, this.password, name, fields);
			return await wpGetPostType.Execute<WordPressPostType>(this);
		}

		/// <summary>
		/// Gets all post types.
		/// </summary>
		/// <param name="filter">Query filter.</param>
		/// <returns>List of post types.</returns>
		public async Task<List<WordPressPostType>> GetPostTypes(WordPressPostTypeFilter filter = null)
		{
			var queryFilter = filter ?? new WordPressPostTypeFilter { Public = true };
			var fields = new[] { "labels", "menu", "taxonomies" };
			var wpGetPostTypes = new WordPressGetPostTypesMethod(
				"wp.getPostTypes",
				this.blogId,
				this.username,
				this.password,
				queryFilter,
				fields);
			return await wpGetPostTypes.Execute<List<WordPressPostType>>(this);
		}

		/// <summary>
		/// Gets all post formats.
		/// </summary>
		/// <returns>List of post formats.</returns>
		public async Task<List<WordPressPostFormat>> GetPostFormats()
		{
			var wpGetPostFormats = new KeyValueStructMethod("wp.getPostFormats", this.blogId, this.username, this.password);
			return await wpGetPostFormats.Execute<List<WordPressPostFormat>>(this);
		}

		/// <summary>
		/// Gets all post statuses.
		/// </summary>
		/// <returns>List of post statuses.</returns>
		public async Task<List<WordPressPostStatus>> GetPostStatusList()
		{
			var wpGetPostStatusList = new KeyValueStructMethod("wp.getPostStatusList", this.blogId, this.username, this.password);
			return await wpGetPostStatusList.Execute<List<WordPressPostStatus>>(this);
		}

		#endregion

		#region Taxonomy Methods

		/// <summary>
		/// Gets a taxonomy.
		/// </summary>
		/// <param name="name">The name.</param>
		/// <returns>A WordPressTaxonomy.</returns>
		public async Task<WordPressTaxonomy> GetTaxonomy(string name)
		{
			var wpGetTaxonomy = new XmlRpcMethod("wp.getTaxonomy", this.blogId, this.username, this.password, name);
			return await wpGetTaxonomy.Execute<WordPressTaxonomy>(this);
		}

		/// <summary>
		/// Gets all taxonomies.
		/// </summary>
		/// <returns>List of taxonomies.</returns>
		public async Task<List<WordPressTaxonomy>> GetTaxonomies()
		{
			var wpGetTaxonomies = new XmlRpcMethod("wp.getTaxonomies", this.blogId, this.username, this.password);
			return await wpGetTaxonomies.Execute<List<WordPressTaxonomy>>(this);
		}

		/// <summary>
		/// Gets a term.
		/// </summary>
		/// <param name="taxonomy">The term taxonomy.</param>
		/// <param name="id">The term id.</param>
		/// <returns>A WordPressTerm.</returns>
		public async Task<WordPressTerm> GetTerm(string taxonomy, string id)
		{
			var wpGetTerm = new XmlRpcMethod("wp.getTerm", this.blogId, this.username, this.password, taxonomy, id);
			return await wpGetTerm.Execute<WordPressTerm>(this);
		}

		/// <summary>
		/// Gets terms.
		/// </summary>
		/// <param name="taxonomy">The taxonomy.</param>
		/// <param name="filter">The filter.</param>
		/// <returns>List of terms.</returns>
		public async Task<List<WordPressTerm>> GetTerms(string taxonomy, WordPressTermFilter filter = null)
		{
			var wpGetTerms = new XmlRpcMethod("wp.getTerms", this.blogId, this.username, this.password, taxonomy, filter);
			return await wpGetTerms.Execute<List<WordPressTerm>>(this);
		}

		/// <summary>
		/// Creates a new term.
		/// </summary>
		/// <param name="term">The term.</param>
		/// <returns>Task for completion of the work. The term will be updated with its new ID once complete.</returns>
		public async Task NewTerm(WordPressTerm term)
		{
			var wpNewTerm = new XmlRpcMethod("wp.newTerm", this.blogId, this.username, this.password, term);
			var termId = await wpNewTerm.Execute<string>(this);
			term.Id = termId;
		}

		#endregion

		#region Media Methods

		/// <summary>
		/// Gets a media item.
		/// </summary>
		/// <param name="id">The media item id.</param>
		/// <returns>A WordPressMediaItem.</returns>
		public async Task<WordPressMediaItem> GetMediaItem(string id)
		{
			var wpGetMediaItem = new XmlRpcMethod("wp.getMediaItem", this.blogId, this.username, this.password, id);
			return await wpGetMediaItem.Execute<WordPressMediaItem>(this);
		}

		/// <summary>
		/// Gets the media library.
		/// </summary>
		/// <param name="filter">Query filters.</param>
		/// <returns>List of media items.</returns>
		public async Task<List<WordPressMediaItem>> GetMediaLibrary(WordPressMediaFilter filter = null)
		{
			var wpGetMediaLibrary = new XmlRpcMethod("wp.getMediaLibrary", this.blogId, this.username, this.password, filter);
			return await wpGetMediaLibrary.Execute<List<WordPressMediaItem>>(this);
		}

		/// <summary>
		/// Uploads a file.
		/// </summary>
		/// <param name="fileUpload">The file upload information.</param>
		/// <returns>A WordPresMediaItem</returns>
		public async Task<WordPressMediaItem> UploadFile(WordPressFileUpload fileUpload)
		{
			var wpUploadFile = new XmlRpcMethod("wp.uploadFile", this.blogId, this.username, this.password, fileUpload);
			var result = await wpUploadFile.Execute<WordPressFileUploadResult>(this);
			return await this.GetMediaItem(result.Id);
		}

		#endregion

		#region Comment Methods

		/// <summary>
		/// Gets the comment count for a post.
		/// </summary>
		/// <param name="post">The post.</param>
		/// <returns>The comment counts.</returns>
		public async Task<WordPressPostCommentCount> GetPostCommentCount(WordPressPost post)
		{
			var wpGetCommentCount = new XmlRpcMethod("wp.getCommentCount", this.blogId, this.username, this.password, post.Id);
			return await wpGetCommentCount.Execute<WordPressPostCommentCount>(this);
		}

		/// <summary>
		/// Gets a comment.
		/// </summary>
		/// <param name="id">The comment ID.</param>
		/// <returns>The comment object.</returns>
		public async Task<WordPressComment> GetComment(string id)
		{
			var wpGetComment = new XmlRpcMethod("wp.getComment", this.blogId, this.username, this.password, id);
			return await wpGetComment.Execute<WordPressComment>(this);
		}

		/// <summary>
		/// Gets comments from the blog.
		/// </summary>
		/// <param name="filter">Query filter.</param>
		/// <returns>List of comments.</returns>
		public async Task<List<WordPressComment>> GetComments(WordPressCommentsFilter filter = null)
		{
			var wpGetComments = new XmlRpcMethod("wp.getComments", this.blogId, this.username, this.password, filter);
			return await wpGetComments.Execute<List<WordPressComment>>(this);
		}

		/// <summary>
		/// Create a new comment
		/// </summary>
		/// <param name="comment">The comment.</param>
		/// <returns>Task for completion of the work. The comment will be updated with its new ID once complete.</returns>
		public async Task NewComment(WordPressComment comment)
		{
			var wpNewComment = new XmlRpcMethod("wp.newComment", this.blogId, this.username, this.password, comment.Post, comment);
			var id = await wpNewComment.Execute<string>(this);
			comment.Id = id;
		}

		/// <summary>
		/// Edits a comment.
		/// </summary>
		/// <param name="comment">The comment.</param>
		/// <returns>Task for completion of the work.</returns>
		public async Task EditComment(WordPressComment comment)
		{
			var wpEditComment = new XmlRpcMethod("wp.editComment", this.blogId, this.username, this.password, comment.Id, comment);
			await wpEditComment.Execute<bool>(this);
		}

		/// <summary>
		/// Deletes a comment.
		/// </summary>
		/// <param name="comment">The comment.</param>
		/// <returns>Task for completion of the work.</returns>
		public async Task DeleteComment(WordPressComment comment)
		{
			var wpDeleteComment = new XmlRpcMethod("wp.deleteComment", this.blogId, this.username, this.password, comment.Id);
			await wpDeleteComment.Execute<bool>(this);
		}

		/// <summary>
		/// Gets all comment statuses
		/// </summary>
		/// <returns>List of comment statuses.</returns>
		public async Task<List<WordPressCommentStatus>> GetCommentStatusList()
		{
			var wpGetCommentStatusList = new KeyValueStructMethod(
				"wp.getCommentStatusList",
				this.blogId,
				this.username,
				this.password);
			return await wpGetCommentStatusList.Execute<List<WordPressCommentStatus>>(this);
		}

		#endregion
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// A WordPress post format
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostFormat : KeyValueBase
	{
	}
}

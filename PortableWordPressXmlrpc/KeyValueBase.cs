﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Base class for key/value types
	/// </summary>
	[XmlRpcSerializable]
	public abstract class KeyValueBase
	{
		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>The name.</value>
		[XmlRpcMember(Name = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the label.
		/// </summary>
		/// <value>The label.</value>
		[XmlRpcMember(Name = "label")]
		public string Label { get; set; }
	}
}

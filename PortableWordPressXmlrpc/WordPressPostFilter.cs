﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Filter options for post methods.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostFilter
	{
		/// <summary>
		/// Gets or sets the type of the post.
		/// </summary>
		/// <value>The type of the post.</value>
		[XmlRpcMember(Name = "post_type", Optional = true)]
		public string PostType { get; set; }

		/// <summary>
		/// Gets or sets the post status.
		/// </summary>
		/// <value>The post status.</value>
		[XmlRpcMember(Name = "post_status", Optional = true)]
		public string PostStatus { get; set; }

		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>The number.</value>
		[XmlRpcMember(Name = "number", Optional = true)]
		public int Number { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>The offset.</value>
		[XmlRpcMember(Name = "offset", Optional = true)]
		public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the ordering field.
		/// </summary>
		/// <value>The ordering field.</value>
		[XmlRpcMember(Name = "orderby", Optional = true)]
		public string OrderBy { get; set; }

		/// <summary>
		/// Gets or sets the ordering direction.
		/// </summary>
		/// <value>The ordering direction.</value>
		[XmlRpcMember(Name = "order", Optional = true)]
		public string OrderDirection { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Metadata for a resized version of a WordPress media item.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressMediaItemSize
	{
		/// <summary>
		/// Filename of this version of the media item, at this size, without the path.
		/// </summary>
		[XmlRpcMember(Name = "file", Optional = true)]
		public string Filename { get; set; }

		/// <summary>
		/// Gets or sets the image width.
		/// </summary>
		[XmlRpcMember(Name = "width", Optional = true)]
		public int Width { get; set; }

		/// <summary>
		/// Gets or sets the image height.
		/// </summary>
		[XmlRpcMember(Name = "height", Optional = true)]
		public int Height { get; set; }

		/// <summary>
		/// Gets or sets the MIME-type of the file.
		/// </summary>
		[XmlRpcMember(Name = "mime-type", Optional = true)]
		public string MimeType { get; set; }
	}
}
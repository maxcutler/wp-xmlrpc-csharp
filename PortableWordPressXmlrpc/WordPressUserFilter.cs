﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Filter options for user methods.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressUserFilter
	{
		/// <summary>
		/// Gets or sets the user role.
		/// </summary>
		/// <value>The role name.</value>
		[XmlRpcMember(Name = "role", Optional = true)]
		public string Role { get; set; }

		/// <summary>
		/// If 'authors', then will return all non-subscriber users.
		/// </summary>
		[XmlRpcMember(Name = "who", Optional = true)]
		public string Who { get; set; }

		/// <summary>
		/// Gets or sets the number.
		/// </summary>
		/// <value>The number.</value>
		[XmlRpcMember(Name = "number", Optional = true)]
		public int Number { get; set; }

		/// <summary>
		/// Gets or sets the offset.
		/// </summary>
		/// <value>The offset.</value>
		[XmlRpcMember(Name = "offset", Optional = true)]
		public int Offset { get; set; }

		/// <summary>
		/// Gets or sets the ordering field.
		/// </summary>
		/// <value>The ordering field.</value>
		[XmlRpcMember(Name = "orderby", Optional = true)]
		public string OrderBy { get; set; }

		/// <summary>
		/// Gets or sets the ordering direction.
		/// </summary>
		/// <value>The ordering direction.</value>
		[XmlRpcMember(Name = "order", Optional = true)]
		public string OrderDirection { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using System.Collections.Generic;

	using PortableXmlrpc;

	/// <summary>
	/// A WordPress post type.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostType
	{
		/// <summary>
		/// Gets or sets the post type name.
		/// </summary>
		/// <value>The post type name.</value>
		[XmlRpcMember(Name = "name")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the post type label.
		/// </summary>
		/// <value>The post type label.</value>
		[XmlRpcMember(Name = "label")]
		public string Label { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type is hierarchical.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type is hierarchical; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "hierarchical")]
		public bool IsHierarchical { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type is public.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type is public; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "public")]
		public bool IsPublic { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type is shown in UI.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type is shown in UI; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "show_ui")]
		public bool ShowUI { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type is built-in.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type is built-in; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "_builtin")]
		public bool IsBuiltin { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type has an archive.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type has an archive; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "has_archive")]
		public bool HasArchive { get; set; }

		/// <summary>
		/// Gets or sets the post type menu position.
		/// </summary>
		/// <value>The post type menu position.</value>
		[XmlRpcMember(Name = "menu_position")]
		public int MenuPosition { get; set; }

		/// <summary>
		/// Gets or sets the post type menu icon path.
		/// </summary>
		/// <value>The post type menu icon path.</value>
		[XmlRpcMember(Name = "menu_icon")]
		public string MenuIconPath { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this post type is shown in menu.
		/// </summary>
		/// <value>
		/// <c>true</c> if this post type is shown in menu; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "show_in_menu")]
		public bool ShowInMenu { get; set; }

		/// <summary>
		/// Gets or sets the taxonomies.
		/// </summary>
		/// <value>The taxonomies.</value>
		[XmlRpcMember(Name = "taxonomies", Optional = true)]
		public List<string> Taxonomies { get; set; }

		/// <summary>
		/// Gets or sets the supported features.
		/// </summary>
		/// <value>The supported features.</value>
		[XmlRpcMember(Name = "supports")]
		public WordPressPostTypeSupports Supports { get; set; }

		/// <summary>
		/// Gets or sets the labels.
		/// </summary>
		/// <value>The labels.</value>
		[XmlRpcMember(Name = "labels")]
		public WordPressPostTypeLabels Labels { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Represents a user's WordPress blog.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressBlog
	{
		/// <summary>
		/// Gets or sets the blog id.
		/// </summary>
		/// <value>
		/// The blog id.
		/// </value>
		[XmlRpcMember(Name = "blogid")]
		public string Id { get; set; }

		/// <summary>
		/// Gets or sets the name.
		/// </summary>
		/// <value>
		/// The name.
		/// </value>
		[XmlRpcMember(Name = "blogName")]
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets the home URL.
		/// </summary>
		/// <value>
		/// The home URL.
		/// </value>
		[XmlRpcMember(Name = "url")]
		public string HomeUrl { get; set; }

		/// <summary>
		/// Gets or sets the XML-RPC URL.
		/// </summary>
		/// <value>
		/// The XML-RPC URL.
		/// </value>
		[XmlRpcMember(Name = "xmlrpc")]
		public string XmlRpcUrl { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this user is an admin.
		/// </summary>
		/// <value>
		///   <c>true</c> if this user is an admin; otherwise, <c>false</c>.
		/// </value>
		[XmlRpcMember(Name = "isAdmin")]
		public bool IsAdmin { get; set; }
	}
}

﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// A WordPress comment status.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressCommentStatus : KeyValueBase
	{
	}
}

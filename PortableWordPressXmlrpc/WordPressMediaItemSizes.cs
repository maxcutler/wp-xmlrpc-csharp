﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Metadata for the different sizes for a WordPress media item.
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressMediaItemSizes
	{
		/// <summary>
		/// Gets or sets the thumbnail size information.
		/// </summary>
		[XmlRpcMember(Name = "thumbnail", Optional = true)]
		public WordPressMediaItemSize Thumbnail { get; set; }

		/// <summary>
		/// Gets or sets the medium size information.
		/// </summary>[XmlRpcMember(Name = "medium", Optional = true)]
		public WordPressMediaItemSize Medium { get; set; }

		/// <summary>
		/// Gets or sets the large size information.
		/// </summary>[XmlRpcMember(Name = "large", Optional = true)]
		public WordPressMediaItemSize Large { get; set; }

		/// <summary>
		/// Gets or sets the post thumbnail size information.
		/// </summary>[XmlRpcMember(Name = "post-thumbnail", Optional = true)]
		public WordPressMediaItemSize PostThumbnail { get; set; }
	}
}
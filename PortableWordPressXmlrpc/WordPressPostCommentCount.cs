﻿namespace PortableWordPressXmlrpc
{
	using PortableXmlrpc;

	/// <summary>
	/// Summary comment counts for a WordPress post
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressPostCommentCount
	{
		[XmlRpcMember(Name = "approved")]
		public int Approved { get; set; }

		[XmlRpcMember(Name = "awaiting_moderation")]
		public int AwaitingModeration { get; set; }

		[XmlRpcMember(Name = "spam")]
		public int Spam { get; set; }

		[XmlRpcMember(Name = "total_comments")]
		public int Total { get; set; }
	}
}

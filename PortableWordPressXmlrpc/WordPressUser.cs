﻿using System;
using PortableXmlrpc;

namespace PortableWordPressXmlrpc
{
	using System.Collections.Generic;

	/// <summary>
	/// Represents a WordPress user
	/// </summary>
	[XmlRpcSerializable]
	public class WordPressUser
	{
		[XmlRpcMember(Name = "user_id", Optional = true)]
		public string Id { get; set; }

		[XmlRpcMember(Name = "username", Optional = true)]
		public string Username { get; set; }

		[XmlRpcMember(Name = "first_name", Optional = true)]
		public string FirstName { get; set; }

		[XmlRpcMember(Name = "last_name", Optional = true)]
		public string LastName { get; set; }

		[XmlRpcMember(Name = "display_name", Optional = true)]
		public string DisplayName { get; set; }

		[XmlRpcMember(Name = "registered", Optional = true)]
		public DateTime? Registered { get; set; }

		[XmlRpcMember(Name = "email", Optional = true)]
		public string Email { get; set; }

		[XmlRpcMember(Name = "nicename", Optional = true)]
		public string NiceName { get; set; }

		[XmlRpcMember(Name = "nickname", Optional = true)]
		public string NickName { get; set; }

		[XmlRpcMember(Name = "url", Optional = true)]
		public string Url { get; set; }

		[XmlRpcMember(Name = "bio", Optional = true)]
		public string Bio { get; set; }

		[XmlRpcMember(Name = "roles", Optional = true)]
		public List<string> Roles { get; set; } 
	}
}

﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetCommentCountTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetCommentCount_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetCommentCount_Response.txt", "Content/Methods")]
		public async Task TestGetCommentCountMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetCommentCount_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetCommentCount_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var post = new WordPressPost { Id = "5" };
			var result = await client.GetPostCommentCount(post);

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(12, result.Approved);
			Assert.AreEqual(1, result.AwaitingModeration);
			Assert.AreEqual(4, result.Spam);
			Assert.AreEqual(17, result.Total);
		}
	}
}

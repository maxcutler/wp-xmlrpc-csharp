﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc;
	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetCommentStatusListTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetCommentStatusList_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetCommentStatusList_Response.txt", "Content/Methods")]
		public async Task TestGetCommentStatusListMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetCommentStatusList_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetCommentStatusList_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetCommentStatusList();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(3, result.Count);
			Assert.AreEqual("hold", result[0].Name);
			Assert.AreEqual("Unapproved", result[0].Label);
			Assert.AreEqual("approve", result[1].Name);
			Assert.AreEqual("spam", result[2].Name);
		}

		[TestMethod]
		[DeploymentItem("Content/Methods/GetCommentStatusList_DeniedResponse.txt", "Content/Methods")]
		public async Task TestGetCommentStatusListMethodPermissionsDenied()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string response = File.ReadAllText("Content/Methods/GetCommentStatusList_DeniedResponse.txt");

			fakeHttpClient.QueueFakeResponse(response);
			try
			{
				await client.GetCommentStatusList();
				Assert.Fail("Should throw fault exception.");
			}
			catch (XmlRpcFaultException ex)
			{
				Assert.AreEqual(403, ex.FaultCode);
			}
		}
	}
}

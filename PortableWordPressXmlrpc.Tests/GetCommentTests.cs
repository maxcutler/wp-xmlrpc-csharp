﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetCommentTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetComment_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetComment_Response.txt", "Content/Methods")]
		public async Task TestGetCommentMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetComment_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetComment_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetComment("100");

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual("100", result.Id);
			Assert.AreEqual("0", result.User);
			Assert.AreEqual("0", result.Parent);
			Assert.AreEqual("hold", result.Status);
			Assert.AreEqual("This is a sample comment.", result.Content);
			Assert.AreEqual("http://www.example.com/2014/01/05/test/#comment-100", result.Link);
			Assert.AreEqual("5", result.Post);
			Assert.AreEqual("Test", result.PostTitle);
			Assert.AreEqual("John Doe", result.AuthorName);
			Assert.AreEqual("http://example.net/", result.AuthorUrl);
			Assert.AreEqual("john@example.net", result.AuthorEmail);
			Assert.AreEqual("123.456.789.012", result.AuthorIpAddress);
			Assert.AreEqual("", result.CommentType);
			Assert.AreEqual(new DateTime(2014, 1, 5, 6, 30, 12), result.CreatedDateTime);
		}
	}
}

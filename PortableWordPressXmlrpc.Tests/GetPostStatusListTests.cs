﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetPostStatusListTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetPostStatusList_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetPostStatusList_Response.txt", "Content/Methods")]
		public async Task TestGetPostStatusListMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetPostStatusList_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetPostStatusList_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetPostStatusList();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(4, result.Count);
			Assert.AreEqual("draft", result[0].Name);
			Assert.AreEqual("Draft", result[0].Label);
			Assert.AreEqual("pending", result[1].Name);
			Assert.AreEqual("Pending Review", result[1].Label);
			Assert.AreEqual("private", result[2].Name);
			Assert.AreEqual("Private", result[2].Label);
			Assert.AreEqual("publish", result[3].Name);
			Assert.AreEqual("Published", result[3].Label);
		}
	}
}

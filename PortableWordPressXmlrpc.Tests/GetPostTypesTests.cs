﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetPostTypesTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetPostTypes_BasicRequest.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetPostTypes_BasicResponse.txt", "Content/Methods")]
		public async Task TestGetPostTypesMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetPostTypes_BasicRequest.txt");
			string response = File.ReadAllText("Content/Methods/GetPostTypes_BasicResponse.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetPostTypes();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(2, result.Count);
			Assert.AreEqual("post", result[0].Name);
			Assert.AreEqual("attachment", result[1].Name);
		}
	}
}

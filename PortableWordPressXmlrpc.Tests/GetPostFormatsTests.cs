﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetPostFormatsTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetPostFormats_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetPostFormats_Response.txt", "Content/Methods")]
		public async Task TestGetPostFormatsMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetPostFormats_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetPostFormats_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetPostFormats();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(10, result.Count);
			Assert.AreEqual("standard", result[0].Name);
			Assert.AreEqual("Standard", result[0].Label);
			Assert.AreEqual("aside", result[1].Name);
		}
	}
}

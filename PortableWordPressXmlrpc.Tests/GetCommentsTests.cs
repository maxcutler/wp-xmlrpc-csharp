﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetCommentsTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetComments_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetComments_Response.txt", "Content/Methods")]
		public async Task TestGetCommentsMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetComments_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetComments_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetComments();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(2, result.Count);
			Assert.AreEqual("100", result[0].Id);
			Assert.AreEqual("101", result[1].Id);
		}

		[TestMethod]
		[DeploymentItem("Content/Methods/GetComments_PagingRequest.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetComments_Response.txt", "Content/Methods")]
		public async Task TestGetCommentsMethodWithPagingFilter()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetComments_PagingRequest.txt");
			string response = File.ReadAllText("Content/Methods/GetComments_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var filter = new WordPressCommentsFilter { Number = 20, Offset = 20 };
			await client.GetComments(filter);

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));
		}

		[TestMethod]
		[DeploymentItem("Content/Methods/GetComments_PostIdRequest.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetComments_Response.txt", "Content/Methods")]
		public async Task TestGetCommentsMethodWithPostFilter()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetComments_PostIdRequest.txt");
			string response = File.ReadAllText("Content/Methods/GetComments_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var filter = new WordPressCommentsFilter { PostId = "5"};
			await client.GetComments(filter);

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));
		}
	}
}

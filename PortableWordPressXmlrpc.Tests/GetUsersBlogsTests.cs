﻿namespace PortableWordPressXmlrpc.Tests
{
	using System;
	using System.IO;
	using System.Threading.Tasks;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;

	[TestClass]
	public class GetUsersBlogsTests
	{
		[TestMethod]
		[DeploymentItem("Content/Methods/GetUsersBlogs_Request.txt", "Content/Methods")]
		[DeploymentItem("Content/Methods/GetUsersBlogs_Response.txt", "Content/Methods")]
		public async Task TestGetUsersBlogsMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string expectedRequest = File.ReadAllText("Content/Methods/GetUsersBlogs_Request.txt");
			string response = File.ReadAllText("Content/Methods/GetUsersBlogs_Response.txt");

			var token = fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetUsersBlogs();

			Assert.AreEqual(expectedRequest, fakeHttpClient.GetRequestMessageContent(token));

			Assert.AreEqual(1, result.Count);

			var blog = result[0];
			Assert.AreEqual("http://www.example.com/", blog.HomeUrl);
			Assert.AreEqual("42", blog.Id);
			Assert.AreEqual(false, blog.IsAdmin);
			Assert.AreEqual("Example blog", blog.Name);
			Assert.AreEqual("http://www.example.com/xmlrpc.php", blog.XmlRpcUrl);
		}

		[TestMethod]
		[DeploymentItem("Content/Methods/GetUsersBlogs_NoBlogsResponse.txt", "Content/Methods")]
		public async Task TestGetUsersBlogsMethodNoBlogs()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new WordPressClient(new Uri("http://foo.com/xmlrpc.php"), "foo", "bar", fakeHttpClient.HttpClient);

			string response = File.ReadAllText("Content/Methods/GetUsersBlogs_NoBlogsResponse.txt");

			fakeHttpClient.QueueFakeResponse(response);
			var result = await client.GetUsersBlogs();

			Assert.AreEqual(0, result.Count);
		}
	}
}

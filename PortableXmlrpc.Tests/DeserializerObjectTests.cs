﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers.SerializationSamples;

	/// <summary>
	/// Tests for object deserialization.
	/// </summary>
	[TestClass]
	public class DeserializerObjectTests
	{
		/// <summary>
		/// Tests deserialization of object lacking XML-RPC serialization attribute.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestNoAttributeClassDeserialization()
		{
			var e = new XElement("struct");
			XmlRpcDeserializer.DeserializeStruct<NoAttributeObject>(e);
		}

		/// <summary>
		/// Tests deserialization of simple object with XML-RPC attribute on class.
		/// </summary>
		[TestMethod]
		public void TestSerializableAttributeDeserialization()
		{
			var e = new XElement("struct");
			var result = XmlRpcDeserializer.DeserializeStruct<OnlyClassAttributeObject>(e);
			Assert.IsInstanceOfType(result, typeof(OnlyClassAttributeObject));
		}

		/// <summary>
		/// Tests deserialization of object using property's name as the XML-RPC name.
		/// </summary>
		[TestMethod]
		public void TestMemberAttributeNoNameDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member", 
					new XElement("name", "FooBar"), 
					new XElement("value", new XElement("string", "zed"))));
			var result = XmlRpcDeserializer.DeserializeStruct<MemberNoNameObject>(e);
			Assert.IsInstanceOfType(result, typeof(MemberNoNameObject));
			Assert.AreEqual("zed", result.FooBar);
		}

		/// <summary>
		/// Tests deserialization of object using custom name for property.
		/// </summary>
		[TestMethod]
		public void TestMemberAttributeNameDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member", 
					new XElement("name", "foobar"), 
					new XElement("value", new XElement("string", "zed"))));
			var result = XmlRpcDeserializer.DeserializeStruct<MemberNameObject>(e);
			Assert.IsInstanceOfType(result, typeof(MemberNameObject));
			Assert.AreEqual("zed", result.FooBar);
		}

		/// <summary>
		/// Tests deserialization of object with both XML-RPC and non-XML-RPC properties.
		/// </summary>
		[TestMethod]
		public void TestMixedMemberAttributesDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member", 
					new XElement("name", "FooBar"), 
					new XElement("value", new XElement("string", "zed"))));
			var result = XmlRpcDeserializer.DeserializeStruct<MixedMembersObject>(e);
			Assert.IsInstanceOfType(result, typeof(MixedMembersObject));
			Assert.AreEqual("zed", result.FooBar);
		}

		/// <summary>
		/// Tests deserialization of object containing another object.
		/// </summary>
		[TestMethod]
		public void TestNestedClassesDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member",
					new XElement("name", "Child"),
					new XElement(
						"value",
						new XElement(
							"struct",
							new XElement(
								"member", 
								new XElement("name", "FooBar"), 
								new XElement("value", new XElement("string", "zed")))))));
			var result = XmlRpcDeserializer.DeserializeStruct<NestedParentObject>(e);
			Assert.IsInstanceOfType(result, typeof(NestedParentObject));
			Assert.IsInstanceOfType(result.Child, typeof(NestedChildObject));
			Assert.AreEqual("zed", result.Child.FooBar);
		}
	}
}

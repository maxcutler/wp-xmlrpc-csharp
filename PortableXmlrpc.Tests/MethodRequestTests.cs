﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Collections.Generic;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	/// <summary>
	/// Tests for XML-RPC request XML generation.
	/// </summary>
	[TestClass]
	public class MethodRequestTests
	{
		/// <summary>
		/// Tests request with no parameters.
		/// </summary>
		[TestMethod]
		public void TestNoArgsMethodRequestXml()
		{
			var method = new XmlRpcMethod("test.noargs");
			var result = method.GenerateRequestXml();
			Assert.AreEqual(
				"<?xml version=\"1.0\"?><methodCall><methodName>test.noargs</methodName><params /></methodCall>",
				result);
		}

		/// <summary>
		/// Tests request with single parameter.
		/// </summary>
		[TestMethod]
		public void TestSimpleArgMethodRequestXml()
		{
			var method = new XmlRpcMethod("test.simplearg", "foobar");
			var result = method.GenerateRequestXml();
			Assert.AreEqual(
				"<?xml version=\"1.0\"?><methodCall><methodName>test.simplearg</methodName><params><param><value><string>foobar</string></value></param></params></methodCall>",
				result);
		}

		/// <summary>
		/// Tests request with multiple, mixed type parameters.
		/// </summary>
		[TestMethod]
		public void TestMixedArgsMethodRequestXml()
		{
			var method = new XmlRpcMethod(
				"test.mixedargs",
				"zed",
				new List<DateTime> { new DateTime(2000, 1, 1), new DateTime(2012, 12, 21) });
			var result = method.GenerateRequestXml();
			Assert.AreEqual(
				"<?xml version=\"1.0\"?><methodCall><methodName>test.mixedargs</methodName><params><param><value><string>zed</string></value></param><param><value><array><data><value><dateTime.iso8601>20000101T00:00:00</dateTime.iso8601></value><value><dateTime.iso8601>20121221T00:00:00</dateTime.iso8601></value></data></array></value></param></params></methodCall>",
				result);
		}
	}
}

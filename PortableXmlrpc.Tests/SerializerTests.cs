﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Collections.Generic;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	/// <summary>
	/// Tests for serialization.
	/// </summary>
	[TestClass]
	public class SerializerTests
	{
		/// <summary>
		/// Tests string serialization.
		/// </summary>
		[TestMethod]
		public void TestStringSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue("foobar").ToString();
			Assert.AreEqual("<string>foobar</string>", result);
		}

		/// <summary>
		/// Tests integer serialization.
		/// </summary>
		[TestMethod]
		public void TestIntSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue(15).ToString();
			Assert.AreEqual("<int>15</int>", result);
		}

		/// <summary>
		/// Tests double serialization.
		/// </summary>
		[TestMethod]
		public void TestDoubleSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue(11.89).ToString();
			Assert.AreEqual("<double>11.89</double>", result);
		}

		/// <summary>
		/// Tests true boolean serialization.
		/// </summary>
		[TestMethod]
		public void TestTrueBooleanSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue(true).ToString();
			Assert.AreEqual("<boolean>1</boolean>", result);
		}

		/// <summary>
		/// Tests false boolean serialization.
		/// </summary>
		[TestMethod]
		public void TestFalseBooleanSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue(false).ToString();
			Assert.AreEqual("<boolean>0</boolean>", result);
		}

		/// <summary>
		/// Tests date time serialization.
		/// </summary>
		[TestMethod]
		public void TestDateTimeSerialization()
		{
			var dt = new DateTime(1998, 7, 17, 14, 8, 55);
			var result = XmlRpcSerializer.SerializeValue(dt).ToString();
			Assert.AreEqual("<dateTime.iso8601>19980717T14:08:55</dateTime.iso8601>", result);
		}

		/// <summary>
		/// Tests array serialization.
		/// </summary>
		[TestMethod]
		public void TestArraySerialization()
		{
			var array = new[] { 1, 3, 5 };
			var result = XmlRpcSerializer.SerializeValue(array).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual(
				"<array><data><value><int>1</int></value><value><int>3</int></value><value><int>5</int></value></data></array>",
				result);
		}

		/// <summary>
		/// Tests array serialization with mixed value types.
		/// </summary>
		[TestMethod]
		public void TestMixedArraySerialization()
		{
			var array = new object[] { 1, "foo", 3.14 };
			var result = XmlRpcSerializer.SerializeValue(array).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual(
				"<array><data><value><int>1</int></value><value><string>foo</string></value><value><double>3.14</double></value></data></array>",
				result);
		}

		/// <summary>
		/// Tests list serialization.
		/// </summary>
		[TestMethod]
		public void TestListSerialization()
		{
			var list = new List<int> { 1, 3, 5 };
			var result = XmlRpcSerializer.SerializeValue(list).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual(
				"<array><data><value><int>1</int></value><value><int>3</int></value><value><int>5</int></value></data></array>",
				result);
		}

		/// <summary>
		/// Tests unsupported serialize type serialization.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestUnsupportedSerializeTypeSerialization()
		{
			XmlRpcSerializer.SerializeValue(new TimeSpan());
		}

		/// <summary>
		/// Tests null serialization.
		/// </summary>
		[TestMethod]
		public void TestNullSerialization()
		{
			var result = XmlRpcSerializer.SerializeValue(null);
			Assert.IsNull(result);
		}
	}
}

﻿namespace PortableXmlrpc.Tests.TestHelpers
{
	using System.Net;
	using System.Net.Http;

	public class FakeHttpClient
	{
		/// <summary>
		/// The fake message handler
		/// </summary>
		private readonly FakeHttpMessageHandler messageHandler;

		/// <summary>
		/// The HTTP client
		/// </summary>
		private readonly HttpClient client;

		/// <summary>
		/// Initializes a new instance of the <see cref="FakeHttpClient"/> class.
		/// </summary>
		public FakeHttpClient()
		{
			this.messageHandler = new FakeHttpMessageHandler();
			this.client = new HttpClient(this.messageHandler, false);
		}

		/// <summary>
		/// Gets the HTTP client.
		/// </summary>
		public HttpClient HttpClient
		{
			get
			{
				return this.client;
			}
		}

		/// <summary>
		/// Queues a fake response.
		/// </summary>
		/// <param name="content">The response content.</param>
		/// <param name="statusCode">The status code.</param>
		/// <returns>Token for the request/response pair.</returns>
		public FakeHttpClientToken QueueFakeResponse(string content, HttpStatusCode statusCode = HttpStatusCode.OK)
		{
			var responseContent = new StringContent(content);
			var responseMessage = new HttpResponseMessage(statusCode) { Content = responseContent };
			this.messageHandler.Responses.Add(responseMessage);
			return new FakeHttpClientToken { Id = this.messageHandler.Responses.Count - 1 };
		}

		/// <summary>
		/// Gets the number of requests that have been made with this client.
		/// </summary>
		/// <returns>Number of requests.</returns>
		public int GetRequestCount()
		{
			return this.messageHandler.Requests.Count;
		}

		/// <summary>
		/// Gets a request made to the fake client.
		/// </summary>
		/// <param name="token">The token returned by QueueFakeResponse.</param>
		/// <returns>The request message.</returns>
		public HttpRequestMessage GetRequestMessage(FakeHttpClientToken token)
		{
			return this.messageHandler.Requests[token.Id];
		}

		/// <summary>
		/// Gets a request content made to the fake client.
		/// </summary>
		/// <param name="token">The token returned by QueueFakeResponse.</param>
		/// <returns>The request message content.</returns>
		public string GetRequestMessageContent(FakeHttpClientToken token)
		{
			return this.messageHandler.RequestContents[token.Id];
		}
	}

	/// <summary>
	/// Token to identify a request/response pair in the fake HTTP client.
	/// </summary>
	public class FakeHttpClientToken
	{
		internal int Id { get; set; }
	}
}

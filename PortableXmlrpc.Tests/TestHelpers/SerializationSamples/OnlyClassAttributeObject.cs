﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute but no properties.
	/// </summary>
	[XmlRpcSerializable]
	internal class OnlyClassAttributeObject
	{
	}
}

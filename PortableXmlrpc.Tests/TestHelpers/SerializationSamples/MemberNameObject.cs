﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute and one XML-RPC property with custom name.
	/// </summary>
	[XmlRpcSerializable]
	internal class MemberNameObject
	{
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[XmlRpcMember(Name = "foobar")]
		public string FooBar { get; set; }
	}
}

﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute that will be nested in another class.
	/// </summary>
	[XmlRpcSerializable]
	internal class NestedChildObject
	{
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[XmlRpcMember]
		public string FooBar { get; set; }
	}
}

﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute and one XML-RPC object property.
	/// </summary>
	[XmlRpcSerializable]
	internal class NestedParentObject
	{
		/// <summary>
		/// Gets or sets the child.
		/// </summary>
		/// <value>The child.</value>
		[XmlRpcMember]
		public NestedChildObject Child { get; set; }
	}
}

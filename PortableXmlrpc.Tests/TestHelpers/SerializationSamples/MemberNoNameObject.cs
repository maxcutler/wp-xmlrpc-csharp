﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute and one XML-RPC property.
	/// </summary>
	[XmlRpcSerializable]
	internal class MemberNoNameObject
	{
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[XmlRpcMember]
		public string FooBar { get; set; }
	}
}

﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute, one XML-RPC property, and one non-XML-RPC property.
	/// </summary>
	[XmlRpcSerializable]
	internal class MixedMembersObject
	{
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The foo bar.</value>
		[XmlRpcMember]
		public string FooBar { get; set; }

		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		public string NotIncluded { get; set; }
	}
}

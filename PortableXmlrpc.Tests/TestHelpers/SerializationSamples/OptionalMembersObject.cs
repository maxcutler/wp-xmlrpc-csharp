﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class with XML-RPC serialization attribute and one optional XML-RPC property with custom name.
	/// </summary>
	[XmlRpcSerializable]
	internal class OptionalMembersObject
	{
		/// <summary>
		/// Gets or sets the value.
		/// </summary>
		/// <value>The value.</value>
		[XmlRpcMember(Name = "foobar", Optional = true)]
		public string FooBar { get; set; }

		/// <summary>
		/// Gets or sets the child value.
		/// </summary>
		/// <value>The child value.</value>
		[XmlRpcMember(Optional = true)]
		public MemberNameObject Child { get; set; }
	}
}

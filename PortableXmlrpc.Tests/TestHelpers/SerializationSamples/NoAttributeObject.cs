﻿namespace PortableXmlrpc.Tests.TestHelpers.SerializationSamples
{
	/// <summary>
	/// Class lacking any XML-RPC serialization attributes.
	/// </summary>
	internal class NoAttributeObject
	{
	}
}

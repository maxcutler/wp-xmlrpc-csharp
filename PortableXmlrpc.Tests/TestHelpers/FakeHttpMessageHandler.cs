﻿namespace PortableXmlrpc.Tests.TestHelpers
{
	using System.Collections.Generic;
	using System.Net.Http;
	using System.Threading;
	using System.Threading.Tasks;

	public class FakeHttpMessageHandler : HttpMessageHandler
	{
		/// <summary>
		/// List of requests that have been sent.
		/// </summary>
		private readonly List<HttpRequestMessage> requests;

		/// <summary>
		/// List of request contents that have been sent.
		/// </summary>
		private readonly List<string> requestContents; 

		/// <summary>
		/// List of responses to return for requests.
		/// </summary>
		private readonly List<HttpResponseMessage> responses;

		/// <summary>
		/// Counter to determine next response to use.
		/// </summary>
		private int nextResponseCount;

		/// <summary>
		/// Initializes a new instance of the <see cref="FakeHttpMessageHandler"/> class.
		/// </summary>
		public FakeHttpMessageHandler()
		{
			this.nextResponseCount = 0;
			this.requests = new List<HttpRequestMessage>();
			this.requestContents = new List<string>();
			this.responses = new List<HttpResponseMessage>();
		}

		/// <summary>
		/// Gets the requests.
		/// </summary>
		public List<HttpRequestMessage> Requests
		{
			get
			{
				return this.requests;
			}
		}

		/// <summary>
		/// Gets the request contents.
		/// </summary>
		public List<string> RequestContents
		{
			get
			{
				return this.requestContents;
			}
		}

		/// <summary>
		/// Gets the responses.
		/// </summary>
		public List<HttpResponseMessage> Responses
		{
			get
			{
				return this.responses;
			}
		}

		/// <summary>
		/// Send an HTTP request as an asynchronous operation.
		/// </summary>
		/// <returns>
		/// Returns <see cref="T:System.Threading.Tasks.Task`1"/>.The task object representing the asynchronous operation.
		/// </returns>
		/// <param name="request">The HTTP request message to send.</param><param name="cancellationToken">The cancellation token to cancel operation.</param><exception cref="T:System.ArgumentNullException">The <paramref name="request"/> was null.</exception>
		protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
		{
			this.requests.Add(request);
			this.requestContents.Add(await request.Content.ReadAsStringAsync());
			var response = this.responses[nextResponseCount++];
			await Task.Yield(); // simulate async
			return response;
		}
	}
}

﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers.SerializationSamples;

	/// <summary>
	/// Tests for object serialization.
	/// </summary>
	[TestClass]
	public class SerializerObjectTests
	{
		/// <summary>
		/// Tests serialization of object lacking XML-RPC serialization attribute.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestNoAttributeClassSerialization()
		{
			var o = new NoAttributeObject();
			XmlRpcSerializer.SerializeValue(o);
		}

		/// <summary>
		/// Tests serialization of simple object with XML-RPC attribute on class.
		/// </summary>
		[TestMethod]
		public void TestSerializableAttributeSerialization()
		{
			var o = new OnlyClassAttributeObject();
			var result = XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual("<struct />", result);
		}

		/// <summary>
		/// Tests serialization of object using property's name as the XML-RPC name.
		/// </summary>
		[TestMethod]
		public void TestMemberAttributeNoNameSerialization()
		{
			var o = new MemberNoNameObject { FooBar = "zed" };
			var result = XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual("<struct><member><name>FooBar</name><value><string>zed</string></value></member></struct>", result);
		}

		/// <summary>
		/// Tests serialization of object using custom name for property.
		/// </summary>
		[TestMethod]
		public void TestMemberAttributeNameSerialization()
		{
			var o = new MemberNameObject { FooBar = "zed" };
			var result = XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual("<struct><member><name>foobar</name><value><string>zed</string></value></member></struct>", result);
		}

		/// <summary>
		/// Tests serialization of object with both XML-RPC and non-XML-RPC properties.
		/// </summary>
		[TestMethod]
		public void TestMixedMemberAttributesSerialization()
		{
			var o = new MixedMembersObject { FooBar = "zed", NotIncluded = "test" };
			var result = XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual("<struct><member><name>FooBar</name><value><string>zed</string></value></member></struct>", result);
		}

		/// <summary>
		/// Tests serialization of object containing another object.
		/// </summary>
		[TestMethod]
		public void TestNestedClassesSerialization()
		{
			var child = new NestedChildObject { FooBar = "zed" };
			var parent = new NestedParentObject { Child = child };
			var result = XmlRpcSerializer.SerializeValue(parent).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual(
				"<struct><member><name>Child</name><value><struct><member><name>FooBar</name><value><string>zed</string></value></member></struct></value></member></struct>",
				result);
		}

		/// <summary>
		/// Tests serialization of object with null non-optional property.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentNullException))]
		public void TestMemberNullSerialization()
		{
			var o = new MemberNameObject();
			XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
		}

		/// <summary>
		/// Tests serialization of object with null optional property.
		/// </summary>
		[TestMethod]
		public void TestOptionalMemberNullSerialization()
		{
			var o = new OptionalMembersObject();
			var result = XmlRpcSerializer.SerializeValue(o).ToString(SaveOptions.DisableFormatting);
			Assert.AreEqual("<struct />", result);
		}
	}
}

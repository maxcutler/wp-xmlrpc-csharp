﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Collections.Generic;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	/// <summary>
	/// Tests for deserialization.
	/// </summary>
	[TestClass]
	public class DeserializerTests
	{
		/// <summary>
		/// Tests string deserialization.
		/// </summary>
		[TestMethod]
		public void TestStringDeserialization()
		{
			var e = new XElement("string", "foobar");
			var result = XmlRpcDeserializer.DeserializeValue<string>(e);
			Assert.IsInstanceOfType(result, typeof(string));
			Assert.AreEqual("foobar", result);
		}

		/// <summary>
		/// Tests integer deserialization.
		/// </summary>
		[TestMethod]
		public void TestIntDeserialization()
		{
			var e = new XElement("int", 15953);
			var result = XmlRpcDeserializer.DeserializeValue<int>(e);
			Assert.IsInstanceOfType(result, typeof(int));
			Assert.AreEqual(15953, result);
		}

		/// <summary>
		/// Tests integer deserialization.
		/// </summary>
		[TestMethod]
		public void TestI4Deserialization()
		{
			var e = new XElement("i4", 9402);
			var result = XmlRpcDeserializer.DeserializeValue<int>(e);
			Assert.IsInstanceOfType(result, typeof(int));
			Assert.AreEqual(9402, result);
		}

		/// <summary>
		/// Tests double deserialization.
		/// </summary>
		[TestMethod]
		public void TestDoubleDeserialization()
		{
			var e = new XElement("double", 3.141596);
			var result = XmlRpcDeserializer.DeserializeValue<double>(e);
			Assert.IsInstanceOfType(result, typeof(double));
			Assert.AreEqual(3.141596, result);
		}

		/// <summary>
		/// Tests true boolean deserialization.
		/// </summary>
		[TestMethod]
		public void TestTrueBooleanDeserialization()
		{
			var e = new XElement("boolean", 1);
			var result = XmlRpcDeserializer.DeserializeValue<bool>(e);
			Assert.IsInstanceOfType(result, typeof(bool));
			Assert.AreEqual(true, result);
		}

		/// <summary>
		/// Tests false boolean deserialization.
		/// </summary>
		[TestMethod]
		public void TestFalseBooleanDeserialization()
		{
			var e = new XElement("boolean", 0);
			var result = XmlRpcDeserializer.DeserializeValue<bool>(e);
			Assert.IsInstanceOfType(result, typeof(bool));
			Assert.AreEqual(false, result);
		}

		/// <summary>
		/// Tests date time deserialization.
		/// </summary>
		[TestMethod]
		public void TestDateTimeDeserialization()
		{
			var e = new XElement("dateTime.iso8601", "1998-07-17T14:08:55");
			var result = XmlRpcDeserializer.DeserializeValue<DateTime>(e);
			Assert.IsInstanceOfType(result, typeof(DateTime));
			Assert.AreEqual(new DateTime(1998, 7, 17, 14, 8, 55), result);
		}

		/// <summary>
		/// Tests array deserialization.
		/// </summary>
		[TestMethod]
		public void TestArrayDeserialization()
		{
			var e = new XElement(
				"array",
				new XElement(
					"data",
					new XElement("value", new XElement("int", -30)),
					new XElement("value", new XElement("string", "foobar"))));
			var result = XmlRpcDeserializer.DeserializeValue<IList<object>>(e);
			Assert.IsInstanceOfType(result, typeof(IList<object>));
			var resultList = (IList<object>)result;
			Assert.AreEqual(-30, resultList[0]);
			Assert.AreEqual("foobar", resultList[1]);
		}
	}
}

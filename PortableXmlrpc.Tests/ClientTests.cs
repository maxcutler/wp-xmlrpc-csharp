﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Net;
	using System.Net.Http;
	using System.Threading.Tasks;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers;
	using PortableXmlrpc.Tests.TestHelpers.SerializationSamples;

	/// <summary>
	/// Summary description for ClientTestscs
	/// </summary>
	[TestClass]
	public class ClientTests
	{
		[TestMethod]
		public async Task TestClientBasicMethod()
		{
			var fakeHttpClient = new FakeHttpClient();
			var client = new XmlRpcClient(new Uri("http://foo.com"), fakeHttpClient.HttpClient);
			var method = new XmlRpcMethod("test");

			var expectedResult = new MemberNameObject { FooBar = "test" };
			var expectedResultXml = XmlRpcSerializer.SerializeValue(expectedResult).ToString(SaveOptions.DisableFormatting);
			var response = "<?xml version=\"1.0\"?><methodResponse><params><param><value>" + expectedResultXml + "</value></param></params></methodResponse>";
			var token = fakeHttpClient.QueueFakeResponse(response);

			var result = await method.Execute<MemberNameObject>(client);

			Assert.AreEqual(1, fakeHttpClient.GetRequestCount());
			Assert.AreEqual("http://foo.com/", fakeHttpClient.GetRequestMessage(token).RequestUri.AbsoluteUri);
			Assert.AreEqual(expectedResult.FooBar, result.FooBar);
		}
	}
}

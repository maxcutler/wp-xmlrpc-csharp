﻿namespace PortableXmlrpc.Tests
{
	using System.Collections.Generic;
	using System.Xml;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	/// <summary>
	/// Tests for parsing XML-RPC response.
	/// </summary>
	[TestClass]
	public class MethodResponseTests
	{
		/// <summary>
		/// A mock method to use for accessing ParseResponse.
		/// </summary>
		private static readonly XmlRpcMethod MockMethod = new XmlRpcMethod("test");

		/// <summary>
		/// Tests empty response.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestEmptyResponse()
		{
			const string Response = "";
			MockMethod.ParseResponse<object>(Response);
		}

		/// <summary>
		/// Tests response with single parameter.
		/// </summary>
		[TestMethod]
		public void TestSingleParamResponse()
		{
			const string Response =
				"<?xml version=\"1.0\"?><methodResponse><params><param><value><boolean>1</boolean></value></param></params></methodResponse>";
			var result = MockMethod.ParseResponse<bool>(Response);
			Assert.AreEqual(true, result);
		}

		/// <summary>
		/// Tests response with single string parameter.
		/// </summary>
		[TestMethod]
		public void TestSingleStringParamResponse()
		{
			const string Response =
				"<?xml version=\"1.0\"?><methodResponse><params><param><value><string>foo</string></value></param></params></methodResponse>";
			var result = MockMethod.ParseResponse<string>(Response);
			Assert.AreEqual("foo", result);
		}

		/// <summary>
		/// Tests response with array parameter.
		/// </summary>
		[TestMethod]
		public void TestArrayParamResponse()
		{
			const string Response =
				"<?xml version=\"1.0\"?><methodResponse><params><param><value><array><data><value><int>1</int></value><value><int>3</int></value></data></array></value></param></params></methodResponse>";
			var result = MockMethod.ParseResponse<List<object>>(Response);
			Assert.AreEqual(1, result[0]);
			Assert.AreEqual(3, result[1]);
		}

		/// <summary>
		/// Tests fault response.
		/// </summary>
		[TestMethod]
		public void TestFaultResponse()
		{
			const string Response =
				"<?xml version=\"1.0\"?><methodResponse><fault><value><struct><member><name>faultCode</name><value><int>4</int></value></member><member><name>faultString</name><value><string>Too many parameters.</string></value></member></struct></value></fault></methodResponse>";
			try
			{
				MockMethod.ParseResponse<object>(Response);
				Assert.Fail("Should have thrown an exception.");
			}
			catch (XmlRpcFaultException ex)
			{
				Assert.AreEqual(4, ex.FaultCode);
				Assert.AreEqual("Too many parameters.", ex.FaultString);
			}
		}

		/// <summary>
		/// Tests fault exception message.
		/// </summary>
		[TestMethod]
		public void TestFaultMessage()
		{
			var fault = new XmlRpcFaultException("Invalid request.", 42);
			Assert.AreEqual("42: Invalid request.", fault.Message);
		}
	}
}

﻿namespace PortableXmlrpc.Tests
{
	using System.Xml;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	[TestClass]
	public class MethodResponseMalformedTests
	{
		/// <summary>
		/// A mock method to use for accessing ParseResponse.
		/// </summary>
		private static readonly XmlRpcMethod MockMethod = new XmlRpcMethod("test");

		/// <summary>
		/// Tests that exception is thrown when the root element is not 'methodResponse'.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestInvalidRootElementThrowsException()
		{
			const string Response = "<?xml version=\"1.0\"?><foo><bar /></foo>";
			MockMethod.ParseResponse<object>(Response);
		}

		/// <summary>
		/// Tests that exception is thrown when the fault response doesn't have a value struct.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestFaultWithoutValueThrowsException()
		{
			const string Response = "<?xml version=\"1.0\"?><methodResponse><fault></fault></methodResponse>";
			MockMethod.ParseResponse<object>(Response);
		}

		/// <summary>
		/// Tests that exception is thrown when the response doesn't have any param values.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestParamsWithoutParamThrowsException()
		{
			const string Response = "<?xml version=\"1.0\"?><methodResponse><params><foo /></params></methodResponse>";
			MockMethod.ParseResponse<object>(Response);
		}

		/// <summary>
		/// Tests that exception is thrown when the response param doesn't have a value struct.
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestParamWithoutValueThrowsException()
		{
			const string Response = "<?xml version=\"1.0\"?><methodResponse><params><param></param></params></methodResponse>";
			MockMethod.ParseResponse<object>(Response);
		}
	}
}

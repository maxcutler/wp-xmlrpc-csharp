﻿namespace PortableXmlrpc.Tests
{
	using System;
	using System.Xml;
	using System.Xml.Linq;

	using Microsoft.VisualStudio.TestTools.UnitTesting;

	using PortableXmlrpc.Tests.TestHelpers.SerializationSamples;

	/// <summary>
	/// Tests for deserializing malformed XML
	/// </summary>
	[TestClass]
	public class DeserializerMalformedTests
	{
		/// <summary>
		/// Tests deserialization of unrecognized element throws exception
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestInvalidValueTypeDeserialization()
		{
			var e = new XElement("foo");
			XmlRpcDeserializer.DeserializeValue<object>(e);
		}

		/// <summary>
		/// Tests array deserialization without 'array' element name
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestInvalidArrayTypeDeserialization()
		{
			var e = new XElement("foo");
			XmlRpcDeserializer.DeserializeArray<object>(e);
		}

		/// <summary>
		/// Tests array deserialization without 'data' element as first child
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestInvalidArrayDataDeserialization()
		{
			var e = new XElement("array", new XElement("foo"));
			XmlRpcDeserializer.DeserializeArray<object>(e);
		}

		/// <summary>
		/// Tests struct deserialization without 'struct' element name
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(ArgumentException))]
		public void TestInvalidStructTypeDeserialization()
		{
			var e = new XElement("foo");
			XmlRpcDeserializer.DeserializeStruct<object>(e);
		}

		/// <summary>
		/// Tests struct deserialization with missing value on member
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestMissingStructMemberValueDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member",
					new XElement("name", "FooBar")));
			XmlRpcDeserializer.DeserializeStruct<MemberNoNameObject>(e);
		}

		/// <summary>
		/// Tests struct deserialization with missing name on member
		/// </summary>
		[TestMethod]
		[ExpectedException(typeof(XmlException))]
		public void TestMissingStructMemberNameDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member",
					new XElement("value", new XElement("string", "zed"))));
			XmlRpcDeserializer.DeserializeStruct<MemberNoNameObject>(e);
		}

		/// <summary>
		/// Tests struct d
		/// </summary>
		[TestMethod]
		public void TestEmptyStringInsteadOfNullDeserialization()
		{
			var e = new XElement(
				"struct",
				new XElement(
					"member",
					new XElement("name", "Child"),
					new XElement("value", new XElement("string", ""))));
			var result = XmlRpcDeserializer.DeserializeStruct<OptionalMembersObject>(e);
			Assert.IsNull(result.Child);
		}
	}
}

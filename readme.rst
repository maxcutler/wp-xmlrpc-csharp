Overview
========

C# library to interface with a WordPress blog's `XML-RPC API`__.

__ http://codex.wordpress.org/XML-RPC_Support

An implementation of the standard WordPress API methods is provided.

Getting Started
---------------

The library's entry point is the `WordPressClient` class.
Pass an `HttpClient` instance and your blog details to initialize the client, then you can invoke methods corresponding to XML-RPC API methods.

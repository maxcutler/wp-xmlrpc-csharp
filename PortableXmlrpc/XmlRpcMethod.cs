﻿namespace PortableXmlrpc
{
	using System.Diagnostics.CodeAnalysis;
	using System.Linq;
	using System.Net.Http;
	using System.Text;
	using System.Threading.Tasks;
	using System.Xml;
	using System.Xml.Linq;

	/// <summary>
	/// XML-RPC method.
	/// </summary>
	[SuppressMessage("StyleCop.CSharp.OrderingRules", "SA1204:StaticElementsMustAppearBeforeInstanceElements",
		Justification = "Code in order of execution during request/response cycle.")]
	public class XmlRpcMethod
	{
		/// <summary>
		/// The name of the method on the XML-RPC server.
		/// </summary>
		private readonly string methodName;

		/// <summary>
		/// The parameters to the XML-RPC method.
		/// </summary>
		private readonly object[] parameters;

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlRpcMethod"/> class.
		/// </summary>
		/// <param name="name">The method name.</param>
		/// <param name="parameters">The method parameters.</param>
		public XmlRpcMethod(string name, params object[] parameters)
		{
			this.methodName = name;
			this.parameters = parameters;
		}

		/// <summary>
		/// Executes the method against a server.
		/// </summary>
		/// <typeparam name="T">The expected return type.</typeparam>
		/// <param name="client">The client configured against a server.</param>
		/// <returns>Result of the XML-RPC method.</returns>
		public async Task<T> Execute<T>(XmlRpcClient client)
		{
			var content = new StringContent(this.GenerateRequestXml(), new UTF8Encoding(), "text/xml");
			var response = await client.HttpClient.PostAsync(client.ServerUri, content);
			var responseContent = await response.Content.ReadAsStringAsync();
			return this.ParseResponse<T>(responseContent);
		}

		/// <summary>
		/// Generates the XML document for the XML-RPC request.
		/// </summary>
		/// <returns>XML content as a string.</returns>
		public string GenerateRequestXml()
		{
			var xml =
				new XDocument(
					new XElement(
						"methodCall",
						new XElement("methodName", this.methodName),
						new XElement(
							"params",
							this.parameters.Select(
								param => new XElement("param", new XElement("value", XmlRpcSerializer.SerializeValue(param)))))));
			return "<?xml version=\"1.0\"?>" + xml.ToString(SaveOptions.DisableFormatting);
		}

		/// <summary>
		/// Parses the raw XML-RPC server response.
		/// </summary>
		/// <typeparam name="T">The expected type of the response.</typeparam>
		/// <param name="response">The raw response.</param>
		/// <returns>Deserialized object from the response.</returns>
		public T ParseResponse<T>(string response)
		{
			var xml = XDocument.Parse(response);

			var methodResponseElement = xml.Element("methodResponse");
			if (methodResponseElement == null)
			{
				throw new XmlException("Malformed XML received from server.");
			}

			var responseType = methodResponseElement.Elements().First();

			if (responseType.Name.LocalName == "fault")
			{
				var faultValueElement = responseType.Element("value");
				if (faultValueElement == null)
				{
					throw new XmlException("Malformed XML received from server.");
				}

				var faultValue = faultValueElement.Elements().First();
				var ex = XmlRpcDeserializer.DeserializeStruct<XmlRpcFaultException>(faultValue);
				throw ex;
			}

			var paramElement = responseType.Element("param");
			if (paramElement == null)
			{
				throw new XmlException("Malformed XML received from server.");
			}

			var valueElement = paramElement.Element("value");
			if (valueElement == null)
			{
				throw new XmlException("Malformed XML received from server.");
			}

			var transformedValue = this.TransformValue(valueElement);
			var responseValue = transformedValue.Elements().First();
			var value = XmlRpcDeserializer.DeserializeValue<T>(responseValue);
			value = value.TryCastListObjects(typeof(T));
			return (T)value;
		}

		/// <summary>
		/// Transforms the value.
		/// Useful for derived classes to convert the raw response into a format that the parser understands.
		/// </summary>
		/// <param name="value">The value.</param>
		/// <returns>Transformed XElement.</returns>
		protected virtual XElement TransformValue(XElement value)
		{
			return value;
		}
	}
}

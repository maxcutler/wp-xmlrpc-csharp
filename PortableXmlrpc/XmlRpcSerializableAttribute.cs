﻿namespace PortableXmlrpc
{
	using System;

	/// <summary>
	/// Marks a class as valid for XML-RPC serialization/deserialization.
	/// </summary>
	[AttributeUsage(AttributeTargets.Class)]
	public class XmlRpcSerializableAttribute : Attribute
	{
	}
}

﻿namespace PortableXmlrpc
{
	using System;

	public enum XmlRpcMemberSerializationMode
	{
		Both,
		SerializeOnly,
		DeserializeOnly
	}

	/// <summary>
	/// Marks an attribute for inclusion in XML-RPC serialization/deserialization.
	/// </summary>
	[AttributeUsage(AttributeTargets.Property)]
	public class XmlRpcMemberAttribute : Attribute
	{
		/// <summary>
		/// Gets or sets the name of the property in XML-RPC.
		/// </summary>
		/// <value>The name.</value>
		public string Name { get; set; }

		/// <summary>
		/// Gets or sets a value indicating whether this <see cref="XmlRpcMemberAttribute" /> is optional.
		/// </summary>
		/// <value>
		///   <c>true</c> if optional; otherwise, <c>false</c>.
		/// </value>
		public bool Optional { get; set; }

		/// <summary>
		/// Gets or sets which serialization modes this member participates in.
		/// </summary>
		public XmlRpcMemberSerializationMode SerializationMode { get; set; }
	}
}

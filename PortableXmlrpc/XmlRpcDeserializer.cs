﻿namespace PortableXmlrpc
{
	using System;
	using System.Collections.Generic;
	using System.Diagnostics;
	using System.Globalization;
	using System.Linq;
	using System.Reflection;
	using System.Xml;
	using System.Xml.Linq;

	/// <summary>
	/// Deserializes objects from XML.
	/// </summary>
	public class XmlRpcDeserializer
	{
		/// <summary>
		/// The supported DateTime string formats.
		/// </summary>
		private static readonly string[] DateFormats = new[] { "s", "yyyyMMdd'T'HH:mm:ss" };

		/// <summary>
		/// Deserializes the XML element to an object.
		/// </summary>
		/// <typeparam name="T">The expected type.</typeparam>
		/// <param name="e">The XML element.</param>
		/// <returns>The object.</returns>
		/// <exception cref="System.ArgumentException">Attempting to deserialize unsupported object type.</exception>
		public static object DeserializeValue<T>(XElement e)
		{
			var elementName = e.Name.LocalName;
			switch (elementName)
			{
				case "string":
					return e.Value;
				case "int":
				case "i4":
					int intValue;
					if (int.TryParse(e.Value, out intValue))
					{
						return intValue;
					}

					return e.Value;
				case "double":
					double doubleValue;
					if (double.TryParse(e.Value, out doubleValue))
					{
						return doubleValue;
					}

					return e.Value;
				case "boolean":
					return e.Value != "0";
				case "dateTime.iso8601":
					DateTime result;
					DateTime.TryParseExact(
						e.Value,
						DateFormats,
						CultureInfo.InvariantCulture,
						DateTimeStyles.AdjustToUniversal,
						out result);
					return result;
				case "array":
					return DeserializeArray<T>(e);
				case "struct":
					return DeserializeStruct<T>(e);
			}

			throw new ArgumentException("Attempting to deserialize unsupported object type: " + elementName);
		}

		/// <summary>
		/// Deserializes an array of objects.
		/// </summary>
		/// <typeparam name="T">The expected type of array elements.</typeparam>
		/// <param name="e">The XML element.</param>
		/// <returns>An IList of objects.</returns>
		/// <exception cref="System.ArgumentException">DeserializeArray must take an array element.</exception>
		public static IList<object> DeserializeArray<T>(XElement e)
		{
			if (e.Name.LocalName != "array")
			{
				throw new ArgumentException("DeserializeArray must take an array element.");
			}

			var dataElement = e.Element("data");
			if (dataElement == null)
			{
				throw new XmlException("Malformed XML received from XML-RPC server.");
			}

			// figure out what kind of IEnumerable T is
			var oType = typeof(T);
			var enumerableType = oType.GetEnumerableElementType();

			// some servers will erroneously return an empty array instead of a null struct
			if (!dataElement.HasElements && enumerableType == null)
			{
				return null;
			}

			// then invoke DeserializeValue using that type instead
			var methodInfo = typeof(XmlRpcDeserializer).GetMethod("DeserializeValue");
			var genericMethodInfo = methodInfo.MakeGenericMethod(enumerableType);
			return
				dataElement.Elements("value")
							.Select(valElement => genericMethodInfo.Invoke(null, new object[] { valElement.Elements().First() }))
							.ToList();
		}

		/// <summary>
		/// Deserializes a struct into an object. 
		/// </summary>
		/// <typeparam name="T">The expected object type.</typeparam>
		/// <param name="e">The XML element.</param>
		/// <returns>The object.</returns>
		/// <exception cref="System.ArgumentException">DeserializeStruct must take a struct element.</exception>
		public static T DeserializeStruct<T>(XElement e)
		{
			if (e.Name.LocalName != "struct")
			{
				throw new ArgumentException("DeserializeStruct must take a struct element.");
			}

			// initializes empty object
			var oType = typeof(T);
			var obj = Activator.CreateInstance(oType);

			if (!oType.IsDefined(typeof(XmlRpcSerializableAttribute), false))
			{
				throw new ArgumentException("Cannot deserialize into class without [XmlRpcSerializable]: " + oType.Name);
			}

			// find all the XML-RPC properties and make a map of their serialized name to the PropertyInfo
			var typeMemberProperties = oType.GetProperties().Where(p => p.IsDefined(typeof(XmlRpcMemberAttribute), true));
			var typeMemberNameMap = new Dictionary<string, PropertyInfo>();
			foreach (var prop in typeMemberProperties)
			{
				var attr = (XmlRpcMemberAttribute)prop.GetCustomAttributes(typeof(XmlRpcMemberAttribute), true)[0];
				var name = attr.Name ?? prop.Name;
				if (attr.SerializationMode != XmlRpcMemberSerializationMode.SerializeOnly)
				{
					typeMemberNameMap.Add(name, prop);
				}
			}

			// find all the members in the XML and set the values on the object
			var xmlMembers = e.Elements("member");
			foreach (var xmlMember in xmlMembers)
			{
				var nameElement = xmlMember.Element("name");
				var valueElement = xmlMember.Element("value");
				if (nameElement == null || valueElement == null)
				{
					throw new XmlException("Malformed XML received from XML-RPC server.");
				}

				var xmlName = nameElement.Value;
				var xmlValue = valueElement.Elements().First();

				if (!typeMemberNameMap.ContainsKey(xmlName))
				{
					continue;
				}

				var prop = typeMemberNameMap[xmlName];

				// use reflection to recurse with appropriate generic type
				var valueType = prop.PropertyType;
				var methodInfo = typeof(XmlRpcDeserializer).GetMethod("DeserializeValue");
				var genericMethodInfo = methodInfo.MakeGenericMethod(valueType);
				var value = genericMethodInfo.Invoke(null, new object[] { xmlValue });
				value = value.TryCastListObjects(valueType);

				var valueNullableType = Nullable.GetUnderlyingType(valueType);
				if (valueNullableType != null)
				{
					valueType = valueNullableType;
					
				}

				// check for expected type
				if (value != null && !value.GetType().IsAssignableFrom(valueType))
				{
					// some servers will return empty string instead of null
					var valueAsString = value as string;
					if (valueAsString != null && valueAsString.Length == 0)
					{
						continue;
					}

					// attempt to coerce numbers from strings
					if (valueAsString != null && valueType == typeof(Int32))
					{
						value = Int32.Parse(valueAsString);
					}
					else if (valueType == typeof(string) && value is int)
					{
						value = value.ToString();
					}

					else
					{
						Debug.WriteLine("Type mistmatch for XML-RPC member: {0}.", xmlName);
						continue;
					}
				}

				prop.SetValue(obj, value, null);
			}

			return (T)obj;
		}
	}
}

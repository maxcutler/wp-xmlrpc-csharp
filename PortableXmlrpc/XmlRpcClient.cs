﻿namespace PortableXmlrpc
{
	using System;
	using System.Net.Http;

	/// <summary>
	/// XML-RPC client connection to a server.
	/// </summary>
	public class XmlRpcClient
	{
		/// <summary>
		/// The server URI.
		/// </summary>
		private readonly Uri serverUri;

		/// <summary>
		/// The client for performing HTTP requests
		/// </summary>
		private readonly HttpClient httpClient;

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlRpcClient" /> class.
		/// </summary>
		/// <param name="serverUri">The server URI.</param>
		/// <param name="httpClient">The HTTP client.</param>
		public XmlRpcClient(Uri serverUri, HttpClient httpClient)
		{
			this.serverUri = serverUri;
			this.httpClient = httpClient;
		}

		/// <summary>
		/// Gets the server URI.
		/// </summary>
		/// <value>The server URI.</value>
		public Uri ServerUri
		{
			get
			{
				return this.serverUri;
			}
		}

		/// <summary>
		/// Gets the HTTP client.
		/// </summary>
		/// <value>The HTTP client.</value>
		public HttpClient HttpClient
		{
			get
			{
				return this.httpClient;
			}
		}
	}
}

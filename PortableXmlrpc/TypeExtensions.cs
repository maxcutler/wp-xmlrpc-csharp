﻿namespace PortableXmlrpc
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Linq;

	/// <summary>
	/// Extensions to <see cref="Type"/> for common reflection logic.
	/// </summary>
	public static class TypeExtensions
	{
		/// <summary>
		/// Determine the generic parameter for an <see cref="IEnumerable"/> type.
		/// </summary>
		/// <param name="t">The IEnumerable type.</param>
		/// <returns>The generic parameter <see cref="Type"/>, or null if t is not an IEnumerable.</returns>
		public static Type GetEnumerableElementType(this Type t)
		{
			var enumerableInterface =
				t.GetInterfaces().FirstOrDefault(ti => ti.IsGenericType && ti.GetGenericTypeDefinition() == typeof(IEnumerable<>));
			if (enumerableInterface != null && t != typeof(string))
			{
				return enumerableInterface.GetGenericArguments()[0];
			}

			return null;
		}

		/// <summary>
		/// Try to convert <see cref="IList"/> of <see cref="object"/> to IList of specific type.
		/// </summary>
		/// <param name="o">The List of objects.</param>
		/// <param name="valueType">The <see cref="Type"/> of the list values.</param>
		/// <returns>A List with more specific generic type, or null if o is not enumerable.</returns>
		public static object TryCastListObjects(this object o, Type valueType)
		{
			var enumerableType = valueType.GetEnumerableElementType();
			if (enumerableType == null)
			{
				return o;
			}

			var valueList = (IList<object>)o;
			var listType = typeof(List<>);
			var genericListType = listType.MakeGenericType(enumerableType);
			var castedValueList = (IList)Activator.CreateInstance(genericListType);
			foreach (var v in valueList)
			{
				castedValueList.Add(v);
			}

			return castedValueList;
		}
	}
}

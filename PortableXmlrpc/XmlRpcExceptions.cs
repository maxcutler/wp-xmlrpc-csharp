﻿namespace PortableXmlrpc
{
	using System;

	/// <summary>
	/// An XML-RPC fault response.
	/// </summary>
	[XmlRpcSerializable]
	public class XmlRpcFaultException : Exception
	{
		/// <summary>
		/// Initializes a new instance of the <see cref="XmlRpcFaultException" /> class.
		/// </summary>
		public XmlRpcFaultException()
		{
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="XmlRpcFaultException" /> class.
		/// </summary>
		/// <param name="faultString">The fault string.</param>
		/// <param name="faultCode">The fault code.</param>
		public XmlRpcFaultException(string faultString, int faultCode)
		{
			this.FaultString = faultString;
			this.FaultCode = faultCode;
		}

		/// <summary>
		/// Gets or sets the fault string.
		/// </summary>
		/// <value>The fault string.</value>
		[XmlRpcMember(Name = "faultString")]
		public string FaultString { get; protected set; }

		/// <summary>
		/// Gets or sets the fault code.
		/// </summary>
		/// <value>The fault code.</value>
		[XmlRpcMember(Name = "faultCode")]
		public int FaultCode { get; protected set; }

		/// <summary>
		/// Gets a message that describes the current exception.
		/// </summary>
		/// <value>The message.</value>
		/// <returns>The error message that explains the reason for the exception, or an empty string("").</returns>
		public override string Message
		{
			get
			{
				return string.Format("{0}: {1}", this.FaultCode, this.FaultString);
			}
		}
	}
}

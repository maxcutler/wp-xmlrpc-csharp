﻿namespace PortableXmlrpc
{
	using System;
	using System.Collections;
	using System.Collections.Generic;
	using System.Globalization;
	using System.Linq;
	using System.Reflection;
	using System.Xml.Linq;

	/// <summary>
	/// Serialize objects to XML.
	/// </summary>
	public class XmlRpcSerializer
	{
		/// <summary>
		/// Serialize an <see cref="object"/> into an <see cref="XElement"/> according to XML-RPC spec.
		/// </summary>
		/// <param name="o">The object.</param>
		/// <returns>The <see cref="XElement"/>.</returns>
		/// <exception cref="ArgumentException">Parameter is of a type that cannot be serialized for XML-RPC.</exception>
		public static XElement SerializeValue(object o)
		{
			if (o == null)
			{
				return null;
			}

			var oType = o.GetType();

			var typeName = oType.Name.ToLowerInvariant();
			switch (typeName)
			{
				case "string":
					return new XElement("string", o);
				case "int32":
					return new XElement("int", o);
				case "double":
					return new XElement("double", o);
				case "boolean":
					return new XElement("boolean", (bool)o ? 1 : 0);
				case "datetime":
					return new XElement("dateTime.iso8601", ((DateTime)o).ToString("yyyyMMdd'T'HH':'mm':'ss", CultureInfo.InvariantCulture));
				case "byte[]":
					return new XElement("base64", Convert.ToBase64String((byte[])o));
			}

			if (oType.IsGenericType && oType.GetGenericTypeDefinition() == typeof(Dictionary<,>))
			{
				var keyType = oType.GetGenericArguments()[0];
				var valueType = oType.GetGenericArguments()[1];

				var methodInfo = typeof(XmlRpcSerializer).GetMethod("SerializeDictionary", BindingFlags.NonPublic | BindingFlags.Static);
				var genericMethodInfo = methodInfo.MakeGenericMethod(keyType, valueType);
				return (XElement)genericMethodInfo.Invoke(null, new[] { o });
			}

			var oEnumerable = o as IEnumerable;
			if (oEnumerable != null)
			{
				var dataEl = new XElement("data");
				foreach (var x in oEnumerable)
				{
					dataEl.Add(new XElement("value", SerializeValue(x)));
				}

				return new XElement("array", dataEl);
			}

			if (oType.IsDefined(typeof(XmlRpcSerializableAttribute), false))
			{
				var structEl = new XElement("struct");
				var members = oType.GetProperties(BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance).Where(p => p.IsDefined(typeof(XmlRpcMemberAttribute), true));
				foreach (var member in members)
				{
					var attr = (XmlRpcMemberAttribute)member.GetCustomAttributes(typeof(XmlRpcMemberAttribute), true)[0];
					var memberName = attr.Name ?? member.Name;
					if (attr.SerializationMode == XmlRpcMemberSerializationMode.DeserializeOnly)
					{
						continue;
					}

					// check for optional value
					var memberValue = member.GetValue(o, null);
					if (memberValue == null)
					{
						if (attr.Optional)
						{
							continue;
						}
						throw new ArgumentNullException(memberName, "Expected XML-RPC member value is null.");
					}

					structEl.Add(
						new XElement("member", new XElement("name", memberName), new XElement("value", SerializeValue(memberValue))));
				}

				return structEl;
			}

			throw new ArgumentException("Attempting to serialize unsupported object type: " + oType.Name);
		}

		/// <summary>
		/// Serialize a dictionary into an XML-RPC struct
		/// </summary>
		/// <typeparam name="TKey">The key type</typeparam>
		/// <typeparam name="TValue">The value type</typeparam>
		/// <param name="dictionary">The dictionary to serialize.</param>
		/// <returns>The <see cref="XElement"/>.</returns>
		private static XElement SerializeDictionary<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
		{
			var structEl = new XElement("struct");
			foreach (var pair in dictionary)
			{
				structEl.Add(
						new XElement("member", new XElement("name", pair.Key.ToString()), new XElement("value", SerializeValue(pair.Value))));
			}

			return structEl;
		}
	}
}
